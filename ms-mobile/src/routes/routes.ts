import { User } from "../models/User";
import { Scheduler } from "../models/Scheduler";
import { OccupationArea } from '../models/OccupationArea';
import { Specialty } from '../models/Specialty';
import { UserProfile } from '../models/UserProfile';
import { SchedulerAppointment } from '../models/SchedulerAppointment';
import { Patient } from '../models/Patient';

const initializeRoutes = (app, URL_SERVER, mongoClient, mongo, transporter, pool) => {

    const url = `mongodb://medx-mongodb:5flGVGPwpMO9yX4KsmT9xO7aHGoHvM7qsh4EmWXZ9ppm40Mmh4rYUG9e64gw9wNRO7doQAqxk2qt7dpbdsC4OA==@${URL_SERVER}/?ssl=true&replicaSet=globaldb`;
    const ObjectId = mongo.ObjectID;
    let codeHistory = 0;

    app.post('/login', async (req, res, next) => {
        const userRequest = req.body;
        mongoClient.connect(url, (err, client) => {
            if (err) {
              console.error(err)
              return
            }
            console.log("Connected successfully to server");
            const db = client.db('medx')
            const collection = db.collection('User')
            collection.find({"user": userRequest.user, "pass": userRequest.pass}).toArray((err, items) => {
                if(err) {
                    client.close();
                    res.status(400).send(JSON.stringify({error: err}));
                    next();
                }
                if(items.length <= 0)
                {
                    client.close();
                    res.status(404).send(JSON.stringify({error: 'user not found'}));
                    next();
                }
                else 
                {
                    client.close();
                    res.status(200).send(JSON.stringify({success: items}));
                }
            });
        })
    })

    app.get('/all', async (req, res, next) => {
        mongoClient.connect(url, (err, client) => {
            if (err) {
              console.error(err)
              return
            }
            console.log("Connected successfully to server");
            const db = client.db('medx')
            const collection = db.collection('User')
            collection.find().toArray((err, items) => {
                if(err) {
                    client.close();
                    res.status(400).send(JSON.stringify({error: err}));
                }

                client.close();
                res.status(200).send(JSON.stringify({success: items}));
            });
        })
    })

    app.post('/add', async (req, res) => {
        const user = req.body as User;
        console.log(user);

        mongoClient.connect(url, (err, client) => {
            if (err) {
              console.error(err)
              return
            }
            console.log("Connected successfully to server");
            const db = client.db('medx')
            const collection = db.collection('User')
            collection.insertOne(user, (err, result) => {
                if(err) {
                    client.close();
                    res.status(400).send(JSON.stringify({error: err}));
                }

                client.close();
                res.status(200).send(JSON.stringify({success: result}));
            });
        })
    })

    app.put('/edit', async (req, res) => {
        const patient = req.body as Patient;
        const id = patient._id;
        delete patient._id;
        console.log("USER", patient);
        mongoClient.connect(url, (err, client) => {
            if (err) {
                console.error(err)
                return
            }
            console.log("Connected successfully to server");
            const db = client.db('medx')
        const collection = db.collection('User')
        console.log({"_id": id});
        collection.update({"_id": new ObjectId(id)}, {$set: {
          "name": patient.name,
          "pass": patient.pass,
          "user": patient.user,
          "birthday": patient.birthday,
          "tel": patient.tel,
          "gender": patient.gender,
          "cpf": patient.cpf,
          "address": patient.address,
          "insurance": patient.insurance,
          "active": patient.active,
          "historic": patient.historic,
        }}, (err, result) => {
            if(err) {
                console.log(err);
                client.close();
                res.status(400).send(JSON.stringify({error: err}));
            }else
            {
                client.close();
                res.status(200).send(JSON.stringify({success: result}));
            }

            });
        })
    })

    app.delete('/delete/:id', async (req, res) => {
        const id = req.params.id;
        mongoClient.connect(url, (err, client) => {
            if (err) {
              console.error(err)
              return
            }
            console.log("Connected successfully to server");
            const db = client.db('medx')
            const collection = db.collection('User')
            collection.deleteOne({"_id": new ObjectId(`${id}`)}, (err, result) => {
                if(err) {
                    client.close();
                    res.status(400).send(JSON.stringify({error: err}));
                }

                client.close();
                res.status(200).send(JSON.stringify({success: result}));
            });
        })
    })

    /** ESQUECI MINHA SENHA */

    app.post('/forgotPassword', async (req, res) => {

        const email = req.body.email;
        const code = Math.floor(Math.random() * 1000);
        codeHistory = code;
        console.log(email, codeHistory);

        const mailOptions = {
            from: 'medxenterprise@gmail.com',
            to: `${email}`,
            subject: 'noreply code password',
            html: `<p>Your code is: ${code}</p>`
        };

        transporter.sendMail(mailOptions, (err, info) => {
            if(err) {
                console.log("ERROR", err)
                res.status(400).send(JSON.stringify({error: err}));
            }
            else{
                console.log(info);
                res.status(200).send({success: 'email enviado'});
            }
         });
    });

    app.get('/codeValidate/:code', async (req, res) => {
        const codeValidate = req.params.code;
        console.log("VALIDATION:", codeValidate)
        console.log("HISTORY:", codeHistory)
        if(codeValidate == codeHistory){
            res.status(200).send({success: true})
        }
        else {
            res.status(403).send({success: false})
        }
    });

    app.post('/changePassword', async (req, res) => {
        const password = req.body.pass;
        const email = req.body.email;

        console.log("EMAIL", email)
        console.log("PASS", password)

        mongoClient.connect(url, (err, client) => {
            if (err) {
              console.error(err)
              return
            }
            console.log("Connected successfully to server");
            const db = client.db('medx')
            const collection = db.collection('User')
            collection.updateOne({"user": `${email}`}, {$set: {"pass": `${password}`}}, (err, result) => {
                if(err) {
                    client.close();
                    res.status(400).send(JSON.stringify({error: err}));
                }
                else if(result.modifiedCount == 0) {
                    client.close();
                    res.status(403).send(JSON.stringify({error: 'user not found'}));
                }
                else
                {
                    client.close();
                    res.status(200).send(JSON.stringify({success: result}));
                }
            });
        })
    })

    app.get('/professional/all', async (req, res) => {
        pool.getConnection((err, connection) => {
            let sql = `CALL GET_ALL_PROFH()`;
            connection.query(sql, true, (err, result, field) => {
              connection.release();
              if (err) {
                res.status(400).send(`Sintaxe error: ${err}`);
              }
              else {
                if (!result) {
                  res.status(400).send(`User not found: ${err}`);
                } else {
                  const users: UserProfile[] = [];
                  result[0].map(item => {
                    if (!users.find(obj => obj.id == item.ID_PROFH)) {
                      const occupationArea: OccupationArea = {
                        COD_AREA: item.COD_AREA,
                        AREA_DESCRIPTION: item.AREA_DESCRIPTION
                      };
                      const specialty: Specialty = {
                        COD_ESPC: item.COD_ESPC,
                        ESPC_DESCRIPTION: item.ESPC_DESCRIPTION
                      };
                      users.push({
                        id: item.ID_PROFH,
                        name: item.PROFH_NAME,
                        username: item.USERNAME,
                        pass: item.PASS,
                        userType: item.USER_TYPE,
                        crm: item.CRM,
                        occupationArea: occupationArea,
                        specialty: specialty,
                        rqe: item.RQE,
                        userGroup: item.ID_GROUP,
                        active: item.ACTIVE,
                        address: [
                          {
                            id: item.ID_ESTABLISHMENT,
                            bairro: item.NEIGHBOURHOOD,
                            cep: item.CEP,
                            cidade: item.CITY,
                            complemento: item.ADDITIONAL,
                            estado: item.STATE,
                            nomeLocal: item.ESTP_NAME,
                            numero: item.ADDRESS_NUMBER,
                            rua: item.STREET
                          }
                        ]
                      });
                    } else {
                      users
                        .find(obj => obj.id == item.ID_PROFH)
                        .address.push({
                          id: item.ID_ESTABLISHMENT,
                          bairro: item.NEIGHBOURHOOD,
                          cep: item.CEP,
                          cidade: item.CITY,
                          complemento: item.ADDITIONAL,
                          estado: item.STATE,
                          nomeLocal: item.ESTP_NAME,
                          numero: item.ADDRESS_NUMBER,
                          rua: item.STREET
                        });
                    }
                  });
                  console.log(users);
                  res.status(200).send(JSON.stringify(users));
                }
              }
            });
          });
    })

    /** SCHEDULER */

      app.get('/schedulers/patient/:id_patient', async (req, res) => {

        const id_patient = req.params.id_patient;
        console.log("ID: ", id_patient);
        console.log("ALL");
        pool.getConnection((err, connection) => {
          let sql = `CALL GET_PATIENT_SCHEDULES("${id_patient}")`;
          connection.query(sql, true, (err, result, field) => {
            connection.release();
            if (err) {
              res.status(400).send(`Schedulers not found: ${err}`);
            }
            else {
              const schedulers: SchedulerAppointment[] = [];
              result[0].map(item => {
                const schedule: SchedulerAppointment = {
                  date: item.SCHEDULE_DAY,
                  hours: [{
                    startTime: item.START_TIME,
                    endTime: item.END_TIME
                  }],
                  id: item.ID_SCHEDULE,
                  professionalId: item.ID_DOCTOR,
                  professionalName: item.DOCTOR_NAME,
                  address: {
                    publicPlace: item.ESTABLISHMENT_NAME,
                    neighborhood: item.ESTABLISHMENT_NEIGHBOURHOOD,
                    numberPlace: item.ESTABLISHMENT_NUMBER,
                    complement: item.ESTABLISHMENT_STREET,
                    cep: item.ESTABLISHMENT_CEP,
                    city: item.ESTABLISHMENT_CITY,
                    state: item.ESTABLISHMENT_STATE
                  },
                  weekDay: ''
                }
                schedulers.push(schedule);
              })
              res.status(200).send(schedulers);
            }
          });
        });
      });
      
      app.post('/scheduler/add', async (req, res) => {
        const scheduler: Scheduler = req.body;
        pool.getConnection((err, connection) => {
            let sql = `CALL MOBILE_GET_SCHEDULE(${scheduler.id}, ${scheduler.idPatient}, '${scheduler.namePatient}', '${scheduler.telPatient}', '${scheduler.genderPatient}')`;
            connection.query(sql, true, (err, result, field) => {
              connection.release();
              if (err) {
                res.status(400).send(`Schedulers not found: ${err}`);
              }
              else {
                res.status(200).send(result[0]);
              }
            });
          });
      });

      app.delete('/scheduler/delete/:id', async (req, res) => {
          const id = req.params.id;
        pool.getConnection((err, connection) => {
            let sql = `CALL MOBILE_SCHEDULING_CANCEL(${id})`;
            connection.query(sql, true, (err, result, field) => {
              connection.release();
              if (err) {
                res.status(400).send(`Sintaxe error: ${err}`);
              }
              else {
                if (!result) {
                  res.status(400).send(`User not found: ${err}`);
                } else {
                  res.status(200).send(JSON.stringify(result[0]));
                }
              }
            });
          });
    })



} 
export default initializeRoutes