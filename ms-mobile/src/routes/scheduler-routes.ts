import { SchedulerAppointment } from '../models/SchedulerAppointment';
import { DefaultJourney } from '../models/DefaultJourney';
import { Appointment } from '../models/Appointment';
import { Hour } from '../models/Hour';

const initializeRoutes = (app, URL_SERVER, mongoClient, mongo, transporter, pool) => {
  /** SCHEDULER */

  app.get('/scheduler/doctor/:id', async (req, res) => {
    const id = req.params.id;
    const id_est = req.params.id_est;

    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `CALL GET_PROFH_SCHEDULE_MOBILE(${id})`;
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(400).send(`Proph Schedules not Found: ${err}`);
        } else {
          const schedulers: SchedulerAppointment[] = [];
          
          schedulers.push(result[0].map((item, indexItem) => {
            
            const hoursReturn: Hour[] = [];
            for (let index = 1; index <= 22; index++) {
              const clausule = index <=9 ? "0"+index : index;
              const clausule2 = (index+1) <=9 ? "0"+(index+1) : (index+1);
              if(item["FLEX_TIME"+clausule+""] !== '' && item["FLEX_TIME"+clausule2+""] !== ''){
                hoursReturn.push({
                  startTime: item["FLEX_TIME"+ clausule +""],
                  endTime: item["FLEX_TIME"+ clausule2 +""]
                })
                index++;
              }
            }

            const scheduler: SchedulerAppointment = {
              id: item.ID,
              professionalId: item.ID_PROFH,
              professionalName: item.PROFH_NAME,
              date: item.DATA_SCH,
              weekDay: item.FK_WEEK_DAY,
              address: {
                cep: item.CEP,
                city: item.CITY,
                complement: item.ADDITIONAL,
                neighborhood: item.NEIGHBOURHOOD,
                numberPlace: item.ADDRESS_NUMBER,
                publicPlace: item.ESTP_NAME,
                state: item.STATE
              },
              hours: hoursReturn
            }
            return scheduler;
          }))
          res.status(200).send(JSON.stringify(schedulers[0]));
        }
      });
    });
  });

  app.post('/appointment/add', async (req, res) => {
    const appointment: Appointment = req.body;

    console.log("ADD APPOINT MICRO: ", appointment);
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `CALL MAKE_APPOINTMENT(${appointment.id_scheduler}, '${appointment.startTime}', '${appointment.endTime}', '${appointment.id_patient}')`;

      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(404).send(`Appointment not added: ${err}`);
        } else {
          res.status(200).send();
        }
      });
    });
  });

  app.delete('/appointment/delete/:id_schedule&:id_patient', async (req, res) => {
    const id_schedule = req.params.id_schedule;
    const id_patient = req.params.id_schedule;
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `CALL CANCEL_APPOINTMENT(${id_schedule},${id_patient})`;
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(404).send(`Appointment not found: ${err}`);
        } else {
          res.status(200).send({ success: true });
        }
      });
    });
  });
};

export default initializeRoutes;
