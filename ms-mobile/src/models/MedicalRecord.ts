import { Prescription } from './Prescription';

export interface MedicalRecord {
    appointmentDate: string;
    startTime: string;
    physicianName: string;
    clinicName: string;
    patientName: string;
    observation: string;
    exames: string;
    examesResults: string;
    prescriptions: Prescription[];
}


const medicalRecord : MedicalRecord = {
    appointmentDate: "01/12/31",
    clinicName: "MEDX2 clinic",
    exames: "tomografia, bla, bla",
    examesResults: "result1, resul2",
    observation: "o paciente esta com doença gravissima",
    patientName: "Mariazinha do braz",
    physicianName: "Doutor Calebe",
    prescriptions: [{
        appointmentDate: "01/12/31",
        clinicName: "MEDX2 clinic",
        doseFrequency: "2 por dia de 8 em 8 horas",
        medicineDose: "um comprimido",
        physicianName: "Doutor Calebe",
        medicineName: "dorflex",
        patientName: "Mariazinha do braz",
        startTime: "08:00"
    }]
}