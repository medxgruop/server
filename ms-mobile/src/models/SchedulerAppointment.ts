import { Adress } from './Adress';
import { Hour } from './Hour';

export interface SchedulerAppointment {
    id?: number;
    professionalId: number;
    professionalName: string;
    address: Adress;
    hours: Hour[];
    date: string;
    weekDay: string;
}