import { UserType } from "./UserType";
import { Address } from "./Adress";
import { OccupationArea } from "./OccupationArea";
import { Specialty } from "./Specialty";

export interface UserProfile {
  id?: number;
  name: string;
  username: string;
  pass: string;
  crm: number;
  rqe: number;
  occupationArea: OccupationArea;
  specialty: Specialty;
  responsability?: string;
  address: Address[];
  userType: number;
  userGroup: number;
  active: boolean;
}
