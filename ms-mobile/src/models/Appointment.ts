export interface Appointment {
    id?: number;
    id_patient: string;
    id_scheduler: number;
    startTime: string;
    endTime: string;
}