import { Adress } from "./Adress";

export interface User {
    _id?: number;
    name: string;
    user: string;
    pass: string;
    token: string;
    bithday: string;
    gender: string;
    adress: Adress;
    CPF: string;
}
