export interface Scheduler {
    id?: number;
    idPatient: number;
    namePatient: string;
    telPatient: string;
    genderPatient: string;
}