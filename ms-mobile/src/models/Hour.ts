export interface Hour {
    startTime: string;
    endTime: string;
}