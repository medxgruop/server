export interface Prescription {
    appointmentDate: string;
    startTime: string;
    physicianName: string;
    clinicName: string;
    patientName: string;
    medicineName: string;
    medicineDose: string;
    doseFrequency: string;
}