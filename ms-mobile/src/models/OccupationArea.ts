export interface OccupationArea {
    COD_AREA: number;
    AREA_DESCRIPTION: string;
}
