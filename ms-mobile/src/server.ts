import routes from './routes/routes';
import schedulerRoutes from './routes/scheduler-routes';

const express = require('express');
const app = express();
const bodyParser = require("body-parser");
const mongoClient = require('mongodb').MongoClient
const mongo = require('mongodb');
const nodemailer = require ('nodemailer');
const mysql = require('mysql');
const port = process.env.PORT || 3003;
const URL_SERVER = `medx-mongodb.documents.azure.com:10255`;


///NodeMailer constructor
const transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
           user: 'medxenterprise@gmail.com',
           pass: 'medxadmin'
       }
   });

//Mysql connection pool
const pool = mysql.createPool({
    connectionLimit: 10, // default = 10
    host: 'medx-database-mysql.mysql.database.azure.com',
    user: 'medxadmin@medx-database-mysql',
    password: "password@123",
    database: "medx_database",
    multipleStatements: true,
    port: 3306,
    ssl: true,
    timeout: 60000,
  });

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.listen(port, '0.0.0.0' , () => console.log(`Server Mobile is running in port: ${port}`))

//route file
routes(app, URL_SERVER, mongoClient, mongo, transporter, pool);
schedulerRoutes(app, URL_SERVER, mongoClient, mongo, transporter, pool)