import { MysqlError } from "mysql";

const mockUser = {
  id: 1,
  user: "admin",
  pass: "admin123"
};

const initializeRoutes = (app: any, jwt: any, pool: any) => {
  /**
   * @swagger
   * /login:
   *  post:
   *      summary: Login no sistema
   *      tags:
   *       - Authenticate
   *      parameters:
   *        - name: User Login
   *          in: body
   *          required: true
   *          schema:
   *              type: object
   *              required:
   *                  - user
   *                  - pass
   *              properties:
   *                  user:
   *                      type: string
   *                  pass:
   *                      type: string
   *                      format: password
   *                  exemple: {
   *                      "user": "someUser",
   *                      "pass": "somePassword"
   *                  }
   *      responses:
   *       200:
   *         description: login
   */
  app.post("/login", (req: any, res: any, next: any) => {
    if (req.body.user === mockUser.user && req.body.pass === mockUser.pass) {
      const user = req.body.user;
      const pass = req.body.pass;
      const token = jwt.sign({ user, pass }, process.env.SECRET, {
        expiresIn: 3600000
      });
      res.status(200).send({ YourToken: token });
    } else {
      pool.getConnection((err: any, connection: any) => {
        let sql = `SELECT PROFH_LOGIN('${req.body.user}','${req.body.pass}') as userFind`;
        // console.log(sql);
        if (err) return res.status(400).send(`Sintaxe error: ${err}`);
        connection.query(
          sql,
          true,
          (err: MysqlError, result: any, field: any) => {
            connection.release();
            if (err) {
              res.status(400).send(`Sintaxe error: ${err}`);
            } else {
              if (!result[0].userFind) {
                res.status(400).send(`User not found: ${err}`);
              } else {
                const user = req.body.user;
                const pass = req.body.pass;
                const token = jwt.sign({ user, pass }, process.env.SECRET, {
                  expiresIn: 3600000
                });
                res.status(200).send({ YourToken: token });
              }
            }
          }
        );
      });
    }
  });
};

export default initializeRoutes;
