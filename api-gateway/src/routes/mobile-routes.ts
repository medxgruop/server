import { Microservices } from "../urls/url-routes";
import { UserMobile } from "../models/mobile/UserMobile";

const initializeRoutes = (app:any, axios:any, verifyJWT:any, jwt: any) => {


    /**
     * @swagger
     * /mobile/login:
     *  post:
     *      summary: Login with your account
     *      tags:
     *          - Mobile
     *      description: use user and pass for login
     *      parameters:
     *          - name: Login
     *            in: body
     *            required: true
     *            schema:
     *              type: object
     *              required:
     *                      - user
     *                      - pass
     *              properties:
     *                      user:
     *                          type: string
     *                      pass:
     *                          type: string
     *                          format: password
     *      responses:
     *          200:
     *              description: Login with soccess
     */
    app.post('/mobile/login', async (req: any, res: any) => {

      axios.post(`http://${Microservices.mobile}/login`, req.body)
      .then( (response:any) => {
        const user = req.body.user;
        const pass = req.body.pass;
        const Usertoken = jwt.sign({user, pass}, process.env.SECRET, {
          expiresIn: 3600000
          });
        res.status(200).send({user: response.data.success[0], token: Usertoken});
      }).
      catch((error:any) => {
        res.status(400).send(error.response.data);
      })
    })

    /**
     * @swagger
     * /mobile/all:
     *  get:
     *      summary: Get all users
     *      tags:
     *        - Mobile
     *      description: Get all users
     *      responses:
     *          200:
     *              description: users returned
     */
    app.get('/mobile/all', async (req:any, res:any) => {
        axios.get(`http://${Microservices.mobile}/all`).
        then((response:any) => {
          res.status(200).send(response.data);
        }).
        catch((error:any) => {
          res.status(400).send(error.response.data);
        })
    });


    /**
     * @swagger
     * /mobile/add:
     *  post:
     *      summary: Create new user
     *      tags:
     *          - Mobile
     *      description: Post user for create new doctor in the database
     *      parameters:
     *          - name: New user
     *            in: body
     *            required: true
     *            schema:
     *              type: object
     *              required:
     *                      - name
     *                      - user
     *                      - pass
     *                      - token
     *                      - birthday
     *                      - gender
     *                      - adress
     *              properties:
     *                      name:
     *                          type: string
     *                      user:
     *                          type: string
     *                      pass:
     *                          type: string
     *                          format: password
     *                      token:
     *                          type: string
     *                      birthday:
     *                          type: string
     *                      gender:
     *                          type: string
     *                      adress:
     *                          type: object
     *      responses:
     *          200:
     *              description: User was added in database
     */
    app.post('/mobile/add', async (req:any, res:any) => {

        axios.post(`http://${Microservices.mobile}/add`, req.body)
        .then( (response:any) => {
          res.status(200).send(response.data);
        }).
        catch((error:any) => {
          res.status(400).send(error.response.data);
        })
    });

        /**
     * @swagger
     * /mobile/edit:
     *  put:
     *      summary: Edit user
     *      tags:
     *          - Mobile
     *      description: Pass atributes requireds and optionals for update user 
     *      parameters:
     *          - name: New user
     *            in: body
     *            required: true
     *            schema:
     *              type: object
     *              required:
     *                      - id
     *                      - name
     *                      - user
     *                      - pass
     *                      - token
     *                      - birthday
     *                      - gender
     *                      - adress
     *              properties:
     *                      id:
     *                        type: number
     *                      name:
     *                          type: string
     *                      user:
     *                          type: string
     *                      pass:
     *                          type: string
     *                          format: password
     *                      token:
     *                          type: string
     *                      birthday:
     *                          type: string
     *                      gender:
     *                          type: string
     *                      adress:
     *                          type: object
     *      responses:
     *          200:
     *              description: User was added in database
     */
    app.put('/mobile/edit', async (req:any, res:any) => {

        axios.put(`http://${Microservices.mobile}/edit`, req.body)
        .then( (response:any) => {
          res.status(200).send(response.data);
        }).
        catch((error:any) => {
          res.status(400).send(error.response.data);
        })

    });

    /**
     * @swagger
     * /mobile/delete:
     *  delete:
     *      summary: Delete an user
     *      tags:
     *          - Mobile
     *      description: Pass an userId and delete him of database
     *      parameters:
     *          - name: userID
     *            in: path
     *            schema:
     *              type: number
     *              required:
     *                      - userId
     *              properties:
     *                      userId:
     *                          type: number
     *      responses:
     *          200:
     *              description: OK
     */
    app.delete('/mobile/delete/:id', async (req:any, res:any) =>{

      axios.delete(`http://${Microservices.mobile}/delete/${req.params.id}`)
      .then( (response:any) => {
        res.status(200).send(response.data);
      }).
      catch((error:any) => {
        res.status(400).send(error.response.data);
      })
        
    });

    app.post('/mobile/forgotPassword', async (req:any, res:any) => {

      axios.post(`http://${Microservices.mobile}/forgotPassword`, req.body)
      .then( (response:any) => {
        res.status(200).send(response.data);
      }).
      catch((error:any) => {
        res.status(400).send(error.response.data);
      })

    });

    app.get('/mobile/codeValidate/:code', async (req:any, res:any) => {
      axios.get(`http://${Microservices.mobile}/codeValidate/${req.params.code}`).
      then((response:any) => {
        res.status(200).send(response.data);
      }).
      catch((error:any) => {
        res.status(400).send(error.response.data);
      })
    });

    app.post('/mobile/changePassword', async (req:any, res:any) => {

      axios.post(`http://${Microservices.mobile}/changePassword`, req.body)
      .then( (response:any) => {
        res.status(200).send(response.data);
      }).
      catch((error:any) => {
        res.status(400).send(error.response.data);
      })

    });

    /** LISTAGEM DE PROFISSIONAIS */
    app.get('/mobile/professional/all', async (req:any, res:any) => {
      axios.get(`http://${Microservices.mobile}/professional/all`).
      then((response:any) => {
        res.status(200).send(response.data);
      }).
      catch((error:any) => {
        res.status(400).send(error.response.data);
      })      
    })


    app.get('/mobile/scheduler/doctor/:id', async (req:any, res:any) =>{

      const id = req.params.id;
      axios.get(`http://${Microservices.mobile}/scheduler/doctor/${id}`)
      .then( (response:any) => {
        res.status(200).send(response.data);
      })
      .catch( (error:any) => {
        res.status(error.response.status).send(error.response.data);
      });
        
    });

    app.post('/mobile/scheduler/add', async (req:any, res:any) => {

      axios.post(`http://${Microservices.mobile}/scheduler/add`, req.body)
      .then( (response:any) => {
        res.status(200).send(response.data);
      }).
      catch((error:any) => {
        res.status(400).send(error.response.data);
      })

    });

    app.get('/mobile/scheduler/delete/:id', async (req:any, res:any) =>{

      const id = req.params.id;
      axios.get(`http://${Microservices.mobile}/scheduler/delete/${id}`)
      .then( (response:any) => {
        res.status(200).send(response.data);
      })
      .catch( (error:any) => {
        res.status(error.response.status).send(error.response.data);
      });
        
    });

    app.post('/mobile/appointment/add', async (req: any, res: any) => {
      console.log("ADD APPOINT GATEWAY");
      axios
        .post(`http://${Microservices.mobile}/appointment/add`, req.body)
        .then((response: any) => {
          res.status(response.status).send(response.data);
        })
        .catch((error: any) => {
          console.log(error);
          res.status(error.response.status).send(error.response.data);
        });
    });
  
    app.delete('/mobile/appointment/delete/:id_schedule&:id_patient', async (req:any, res:any) =>{
  
      const id_schedule = req.params.id_schedule;
      const id_patient = req.params.id_schedule;
      axios.delete(`http://${Microservices.mobile}/appointment/delete/${id_schedule}&${id_patient}`)
      .then( (response:any) => {
        res.status(200).send(response.data);
      })
      .catch( (error:any) => {
        res.status(error.response.status).send(error.response.data);
      });
        
    });

}

export default initializeRoutes;