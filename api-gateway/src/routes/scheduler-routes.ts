import { Microservices } from '../urls/url-routes';
import { DefaultJourney } from './DefaultJourney';
import { DoctorScheduler } from '../models/web/DoctorScheduler';
import { AddAgenda } from '../models/web/AddAgenda';
import { Appointment } from '../models/web/Appointment';

const initializeRoutes = (app: any, axios: any, verifyJWT: any) => {
  app.get(
    '/user/scheduler/doctor/:id',
    verifyJWT,
    async (req: any, res: any) => {
      const id = req.params.id;
      axios
        .get(`http://${Microservices.user}/scheduler/doctor/${id}`)
        .then(async (response: any) => {
          await res.status(200).send(response.data);
        })
        .catch((error: any) => {
          res.status(error.response.status).send(error.response.data);
        });
    }
  );

  app.post(
    '/user/scheduler/default/add',
    verifyJWT,
    async (req: any, res: any) => {
      const jorney: DefaultJourney = req.body;
      axios
        .post(`http://${Microservices.user}/scheduler/default/add`, jorney)
        .then((response: any) => {
          res.status(response.status).send(response.data);
        })
        .catch((error: any) => {
          console.log(error);
          res.status(error.response.status).send(error.response.data);
        });
    }
  );

  app.post('/user/scheduler/add', verifyJWT, async (req: any, res: any) => {
    const jorney: AddAgenda = req.body as AddAgenda;

    axios
      .post(`http://${Microservices.user}/scheduler/add`, jorney)
      .then((response: any) => {
        res.status(response.status).send(response.data);
      })
      .catch((error: any) => {
        console.log(error);
        res.status(error.response.status).send(error.response.data);
      });
  });

  app.post('/user/appointment/add', verifyJWT, async (req: any, res: any) => {
    const appt: Appointment = req.body;

    axios
      .post(`http://${Microservices.user}/appointment/add`, appt)
      .then((response: any) => {
        res.status(response.status).send(response.data);
      })
      .catch((error: any) => {
        console.log(error);
        res.status(error.response.status).send(error.response.data);
      });
  });

  app.delete('/user/appointment/delete/:id_sched&:id_patient', verifyJWT, async (req: any, res: any) => {
    const id_schedule = req.params.id_sched;
    const id_patient = req.params.id_patient;
    console.log("IDS+>> scheduler: ", id_schedule, " patient: ", id_patient);
    axios
      .delete(`http://${Microservices.user}/appointment/delete/${id_schedule}&${id_patient}`)
      .then((response: any) => {
        res.status(response.status).send(response.data);
      })
      .catch((error: any) => {
        console.log(error);
        res.status(error.response.status).send(error.response.data);
      });
  });
  
  app.get(
    '/user/scheduler/default/isEnable/:id&:id_est',
    verifyJWT,
    async (req: any, res: any) => {
      const id = req.params.id;
      const id_est = req.params.id_est;
      axios
        .get(`http://${Microservices.user}/scheduler/default/isEnable/${id}&${id_est}`)
        .then((response: any) => {
          res.status(200).send(response.data);
        })
        .catch((error: any) => {
          res.status(error.response.status).send(error.response.data);
        });
    }
  );

  app.get(
    '/mobile/schedulers/patient/:id_patient',
    async (req: any, res: any) => {
      const id_patient = req.params.id_patient;
      axios
        .get(`http://${Microservices.mobile}/schedulers/patient/${id_patient}`)
        .then((response: any) => {
          res.status(200).send(response.data);
        })
        .catch((error: any) => {
          res.status(error.response.status).send(error.response.data);
        });
    }
  );

};

export default initializeRoutes;
