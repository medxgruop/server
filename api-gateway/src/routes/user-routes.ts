import { Microservices } from '../urls/url-routes';
import { UserProfile } from '../models/web/UserProfile';
import { Group } from '../models/web/Group';
import { Patient } from '../models/web/Patient';
import { AddAgenda } from '../models/web/AddAgenda';
import { Address } from '../models/web/Adress';
import { Prescription } from '../models/web/Prescription';
import { MedicalRecord } from '../models/web/MedicalRecord';

const initializeRoutes = (app: any, axios: any, verifyJWT: any) => {
  /**
   * @swagger
   * /user/self:
   *  get:
   *      summary: Usuário logado
   *      tags:
   *        - User
   *      description: Get your user
   *      responses:
   *          200:
   *              description: user returned
   */
  app.get('/user/self', verifyJWT, async (req: any, res: any) => {
    const userLogin = {
      user: `${req.userLogin.user}`
    };
    axios
      .get(`http://${Microservices.user}/self`, { headers: userLogin })
      .then((response: any) => {
        res.status(200).send(response.data[0]);
      })
      .catch((error: any) => {
        res.status(error.response.status).send(error.response.data);
      });
  });

  /**
   * @swagger
   * /user/all:
   *  get:
   *      summary: Get all users and address
   *      tags:
   *        - User
   *      description: Get all users
   *      responses:
   *          200:
   *              description: users returned
   */
  app.get('/user/all', verifyJWT, async (req: any, res: any) => {
    axios
      .get(`http://${Microservices.user}/all`)
      .then((response: any) => {
        res.status(200).send(response.data);
      })
      .catch((error: any) => {
        res.status(error.response.status).send(error.response.data);
      });
  });

  /**
   * @swagger
   * /user/add:
   *  post:
   *      summary: Create new user
   *      tags:
   *          - User
   *      description: Post user for create new doctor in the database
   *      parameters:
   *          - name: New user
   *            in: body
   *            required: true
   *            schema:
   *              type: object
   *              required:
   *                      - name
   *                      - username
   *                      - pass
   *                      - crh
   *                      - rqe
   *                      - occupationArea
   *                      - especialty
   *                      - cep
   *                      - nomeLocal
   *                      - numero
   *                      - complemento
   *                      - rua
   *                      - bairro
   *                      - cidade
   *                      - estado
   *                      - accessLevel
   *              properties:
   *                      name:
   *                          type: string
   *                      username:
   *                          type: string
   *                      pass:
   *                          type: string
   *                          format: password
   *                      crh:
   *                          type: number
   *                      rqe:
   *                          type: number
   *                      occupationArea:
   *                          type: number
   *                      especialty:
   *                          type: number
   *                      cep:
   *                          type: string
   *                      nomeLocal:
   *                          type: string
   *                      numero:
   *                          type: string
   *                      complemento:
   *                          type: string
   *                      rua:
   *                          type: string
   *                      bairro:
   *                          type: string
   *                      cidade:
   *                          type: string
   *                      estado:
   *                          type: string
   *                      accessLevel:
   *                          type: number
   *      responses:
   *          200:
   *              description: User was added in database
   */
  app.post('/user/add/doctor', verifyJWT, async (req: any, res: any) => {
    const user: UserProfile = req.body as UserProfile;

    axios
      .post(`http://${Microservices.user}/add/doctor`, user)
      .then((response: any) => {
        res.status(response.status).send(response.data);
      })
      .catch((error: any) => {
        console.log(error);
        res.status(error.response.status).send(error.response.data);
      });
  });

  /**
   * @swagger
   * /user/add:
   *  post:
   *      summary: Create new user
   *      tags:
   *          - User
   *      description: Post user for create new doctor in the database
   *      parameters:
   *          - name: New user
   *            in: body
   *            required: true
   *            schema:
   *              type: object
   *              required:
   *                      - name
   *                      - username
   *                      - pass
   *                      - crh
   *                      - rqe
   *                      - occupationArea
   *                      - especialty
   *                      - cep
   *                      - nomeLocal
   *                      - numero
   *                      - complemento
   *                      - rua
   *                      - bairro
   *                      - cidade
   *                      - estado
   *                      - accessLevel
   *              properties:
   *                      name:
   *                          type: string
   *                      username:
   *                          type: string
   *                      pass:
   *                          type: string
   *                          format: password
   *                      cep:
   *                          type: string
   *                      nomeLocal:
   *                          type: string
   *                      numero:
   *                          type: string
   *                      complemento:
   *                          type: string
   *                      rua:
   *                          type: string
   *                      bairro:
   *                          type: string
   *                      cidade:
   *                          type: string
   *                      estado:
   *                          type: string
   *                      accessLevel:
   *                          type: number
   *      responses:
   *          200:
   *              description: User was added in database
   */
  app.post('/user/add/operational', verifyJWT, async (req: any, res: any) => {
    const user: UserProfile = req.body as UserProfile;

    axios
      .post(`http://${Microservices.user}/add/operational`, user)
      .then((response: any) => {
        res.status(response.status).send(response.data);
      })
      .catch((error: any) => {
        console.log(error);
        res.status(error.response.status).send(error.response.data);
      });
  });

  app.post('/user/add/address', verifyJWT, async (req: any, res: any) => {
    const address: Address = req.body as Address;

    axios
      .post(`http://${Microservices.user}/add/address`, address)
      .then((response: any) => {
        res.status(response.status).send(response.data);
      })
      .catch((error: any) => {
        console.log(error);
        res.status(error.response.status).send(error.response.data);
      });
  });

  /**
   * @swagger
   * /user/edit:
   *  put:
   *      summary: Edit user
   *      tags:
   *          - User
   *      description: Pass atributes requireds and optionals for update user
   *      parameters:
   *          - name: Edit user
   *            in: body
   *            required: true
   *            schema:
   *              type: object
   *              required:
   *                      - name
   *                      - username
   *                      - pass
   *                      - crh
   *                      - rqe
   *                      - occupationArea
   *                      - especialty
   *                      - cep
   *                      - nomeLocal
   *                      - numero
   *                      - complemento
   *                      - rua
   *                      - bairro
   *                      - cidade
   *                      - estado
   *                      - accessLevel
   *              properties:
   *                      name:
   *                          type: string
   *                      username:
   *                          type: string
   *                      pass:
   *                          type: string
   *                          format: password
   *                      crh:
   *                          type: number
   *                      rqe:
   *                          type: number
   *                      occupationArea:
   *                          type: number
   *                      especialty:
   *                          type: number
   *                      cep:
   *                          type: string
   *                      nomeLocal:
   *                          type: string
   *                      numero:
   *                          type: string
   *                      complemento:
   *                          type: string
   *                      rua:
   *                          type: string
   *                      bairro:
   *                          type: string
   *                      cidade:
   *                          type: string
   *                      estado:
   *                          type: string
   *                      accessLevel:
   *                          type: number
   *      responses:
   *          200:
   *              description: User was added in database
   */
  app.put('/user/edit', verifyJWT, async (req: any, res: any) => {
    const user: UserProfile = req.body as UserProfile;

    axios
      .put(`http://${Microservices.user}/edit`, user)
      .then((response: any) => {
        res.status(200).send(response.data);
      })
      .catch((error: any) => {
        res.status(error.response.status).send(error.response.data);
      });
  });

  /**
   * @swagger
   * /user/delete:
   *  delete:
   *      summary: Delete an user
   *      tags:
   *          - User
   *      description: Pass an userId and delete him of database
   *      parameters:
   *          - name: userID
   *            in: path
   *            schema:
   *              type: number
   *              required:
   *                      - userId
   *              properties:
   *                      userId:
   *                          type: number
   *      responses:
   *          200:
   *              description: OK
   */
  app.delete('/user/delete/:id&:active&:type', verifyJWT, async (req: any, res: any) => {
    axios
      .delete(`http://${Microservices.user}/delete/${req.params.id}&${req.params.active}&:${req.params.type}`)
      .then((response: any) => {
        res.status(200).send(response.data);
      })
      .catch((error: any) => {
        res.status(error.response.status).send(error.response.data);
      });
  });

  /**
   * @swagger
   * /user/occupation:
   *  get:
   *    summary: Get all occupation areas
   *    tags:
   *        - User
   *    description: Get occupation areas from form
   *    responses:
   *        200:
   *            description: OK
   */
  app.get('/user/occupation', verifyJWT, async (req: any, res: any) => {
    axios
      .get(`http://${Microservices.user}/occupation`)
      .then((response: any) => {
        res.status(200).send(response.data);
      })
      .catch((error: any) => {
        res.status(error.response.status).send(error.response.data);
      });
  });

  /**
   * @swagger
   * /user/specialty:
   *  get:
   *    summary: Get all specialty areas
   *    tags:
   *        - User
   *    description: Get specialty areas from form
   *    responses:
   *        200:
   *            description: OK
   */
  app.get('/user/specialty', verifyJWT, async (req: any, res: any) => {
    axios
      .get(`http://${Microservices.user}/specialty`)
      .then((response: any) => {
        res.status(200).send(response.data);
      })
      .catch((error: any) => {
        res.status(error.response.status).send(error.response.data);
      });
  });

  /**
   * @swagger
   * /user/checkUsername:
   *  get:
   *    summary: Check if username exist
   *    tags:
   *        - User
   *    description: Check if username exist
   *    responses:
   *        200:
   *            description: OK
   */
  app.get(
    '/user/checkUsername/:user',
    verifyJWT,
    async (req: any, res: any) => {
      axios
        .get(`http://${Microservices.user}/checkUsername/${req.params.user}`)
        .then((response: any) => {
          res.status(200).send(response.data);
        })
        .catch((error: any) => {
          res.status(error.response.status).send(error.response.data);
        });
    }
  );

  app.post('/user/group/add', verifyJWT, async (req: any, res: any) => {
    const group: Group = req.body;
    console.log('NOVO GRUPO');

    axios
      .post(`http://${Microservices.user}/group/add`, group)
      .then((response: any) => {
        res.status(200).send(response.data);
      })
      .catch((error: any) => {
        console.log(error);
        res.status(error.response.status).send(error.response.data);
      });
  });

  app.put('/user/group/edit', verifyJWT, async (req: any, res: any) => {
    const group: Group = req.body;

    axios
      .put(`http://${Microservices.user}/group/edit`, group)
      .then((response: any) => {
        res.status(200).send(response.data);
      })
      .catch((error: any) => {
        console.log(error);
        res.status(error.response.status).send(error.response.data);
      });
  });

  app.put(
    '/user/group/associateUser',
    verifyJWT,
    async (req: any, res: any) => {
      const groupAss = req.body;

      axios
        .put(`http://${Microservices.user}/group/associateUser`, groupAss)
        .then((response: any) => {
          res.status(200).send(response.data);
        })
        .catch((error: any) => {
          console.log(error);
          res.status(error.response.status).send(error.response.data);
        });
    }
  );

  app.delete(
    '/user/group/delete/:id',
    verifyJWT,
    async (req: any, res: any) => {
      const id = req.params.id;
      axios
        .delete(`http://${Microservices.user}/group/delete/${id}`)
        .then((response: any) => {
          res.status(200).send(response.data);
        })
        .catch((error: any) => {
          res.status(error.response.status).send(error.response.data);
        });
    }
  );

  app.get('/user/groups', verifyJWT, async (req: any, res: any) => {
    axios
      .get(`http://${Microservices.user}/groups`)
      .then((response: any) => {
        res.status(200).send(response.data);
      })
      .catch((error: any) => {
        res.status(error.response.status).send(error.response.data);
      });
  });

  app.get('/user/group/:id', verifyJWT, async (req: any, res: any) => {
    const id = req.params.id;
    console.log('ID API GATEWAY', id);
    axios
      .get(`http://${Microservices.user}/group/${id}`)
      .then((response: any) => {
        res.status(200).send(response.data);
      })
      .catch((error: any) => {
        console.log('ERROR API: ', error.response.data);
        res.status(error.response.status).send(error.response.data);
      });
  });

  app.get('/user/group/getCRM/:crm', verifyJWT, async (req: any, res: any) => {
    const crm = req.params.crm;
    const keyAPi = 2537296081;
    const urlCRM = `https://www.consultacrm.com.br/api/index.php?tipo=crm&uf=&q=${crm}&chave=${keyAPi}&destino=json`;
    axios
      .get(urlCRM)
      .then((response: any) => {
        res.status(200).send(response.data.item);
      })
      .catch((error: any) => {
        res.status(error.response.status).send(error.response.data);
      });
  });

  app.get('/user/functionalities', verifyJWT, async (req: any, res: any) => {
    axios
      .get(`http://${Microservices.user}/functionalities`)
      .then((response: any) => {
        res.status(200).send(response.data);
      })
      .catch((error: any) => {
        res.status(error.response.status).send(error.response.data);
      });
  });

  /** PATIENTS */

  app.post('/user/patient/add', verifyJWT, async (req: any, res: any) => {
    const patient: Patient = req.body;
    console.log(patient);

    axios
      .post(`http://${Microservices.user}/patient/add`, patient)
      .then((response: any) => {
        res.status(200).send(response.data);
      })
      .catch((error: any) => {
        console.log(error);
        res.status(error.response.status).send(error.response.data);
      });
  });

  app.put('/user/patient/edit', verifyJWT, async (req: any, res: any) => {
    axios
      .put(`http://${Microservices.user}/patient/edit`, req.body)
      .then((response: any) => {
        res.status(200).send(response.data);
      })
      .catch((error: any) => {
        console.log(error);
        res.status(error.response.status).send(error.response.data);
      });
  });

  app.put('/user/patient/prescription/add', verifyJWT, async (req: any, res: any) => {
    const prescription: Prescription = req.body;

    axios
      .put(`http://${Microservices.user}/patient/prescription/add`, prescription)
      .then((response: any) => {
        res.status(200).send(response.data);
      })
      .catch((error: any) => {
        console.log(error);
        res.status(error.response.status).send(error.response.data);
      });
  });

  app.post('/user/patient/medical/add/:id_patient', verifyJWT, async (req: any, res: any) => {
    const medicalRecord: MedicalRecord = req.body;

    axios
      .post(`http://${Microservices.user}/patient/medical/add/${req.params.id_patient}`, medicalRecord)
      .then((response: any) => {
        res.status(200).send(response.data);
      })
      .catch((error: any) => {
        console.log(error);
        res.status(error.response.status).send(error.response.data);
      });
  });

  app.delete(
    '/user/patient/delete/:id',
    verifyJWT,
    async (req: any, res: any) => {
      const id = req.params.id;
      axios
        .delete(`http://${Microservices.user}/patient/delete/${id}`)
        .then((response: any) => {
          res.status(200).send(response.data);
        })
        .catch((error: any) => {
          res.status(error.response.status).send(error.response.data);
        });
    }
  );

  app.get('/user/patients', verifyJWT, async (req: any, res: any) => {
    axios
      .get(`http://${Microservices.user}/patients`)
      .then((response: any) => {
        res.status(200).send(response.data);
      })
      .catch((error: any) => {
        console.log(error);
        res.status(error.response.status).send(error.response.data);
      });
  });

  app.get('/user/patient/historic/:id', verifyJWT, async (req: any, res: any) => {
    axios
      .get(`http://${Microservices.user}/patient/historic/${req.params.id}`)
      .then((response: any) => {
        res.status(200).send(response.data);
      })
      .catch((error: any) => {
        console.log(error);
        res.status(error.response.status).send(error.response.data);
      });
  });

  /** SCHEDULER */

  app.post('/user/scheduler/add', verifyJWT, async (req: any, res: any) => {
    const scheduler: AddAgenda = req.body;
    console.log(scheduler);

    axios
      .post(`http://${Microservices.user}/scheduler/add`, scheduler)
      .then((response: any) => {
        res.status(200).send(response.data);
      })
      .catch((error: any) => {
        console.log(error);
        res.status(error.response.status).send(error.response.data);
      });
  });

  app.get('/user/schedulers/:id', verifyJWT, async (req: any, res: any) => {
    const id = req.params.id;
    axios
      .get(`http://${Microservices.user}/schedulers/${id}`)
      .then((response: any) => {
        res.status(200).send(response.data);
      })
      .catch((error: any) => {
        res.status(error.response.status).send(error.response.data);
      });
  });

  app.post('/user/scheduler/delete', verifyJWT, async (req: any, res: any) => {
    axios
      .post(`http://${Microservices.user}/scheduler/delete/`, req.body)
      .then((response: any) => {
        res.status(200).send(response.data);
      })
      .catch((error: any) => {
        res.status(error.response.status).send(error.response.data);
      });
  });

  app.get('/user/establishments', verifyJWT, async (req: any, res: any) => {
    axios
      .get(`http://${Microservices.user}/establishments`)
      .then((response: any) => {
        res.status(200).send(response.data);
      })
      .catch((error: any) => {
        res.status(error.response.status).send(error.response.data);
      });
  });

  app.get(
    '/user/proph/establishments/:id',
    verifyJWT,
    async (req: any, res: any) => {
      const id = req.params.id;
      axios
        .get(`http://${Microservices.user}/proph/establishments/${id}`)
        .then((response: any) => {
          res.status(200).send(response.data);
        })
        .catch((error: any) => {
          res.status(error.response.status).send(error.response.data);
        });
    }
  );

  app.get(
    '/user/establishments/proph/:id',
    verifyJWT,
    async (req: any, res: any) => {
      const id = req.params.id;
      axios
        .get(`http://${Microservices.user}/establishments/proph/${id}`)
        .then((response: any) => {
          res.status(200).send(response.data);
        })
        .catch((error: any) => {
          res.status(error.response.status).send(error.response.data);
        });
    }
  );

  app.get(
    '/user/profops/establishments/:id',
    verifyJWT,
    async (req: any, res: any) => {
      const id = req.params.id;
      axios
        .get(`http://${Microservices.user}/profops/establishments/${id}`)
        .then((response: any) => {
          res.status(200).send(response.data);
        })
        .catch((error: any) => {
          res.status(error.response.status).send(error.response.data);
        });
    }
  );

  app.get(
    '/user/establishments/profops/:id',
    verifyJWT,
    async (req: any, res: any) => {
      const id = req.params.id;
      axios
        .get(`http://${Microservices.user}/establishments/profops/${id}`)
        .then((response: any) => {
          res.status(200).send(response.data);
        })
        .catch((error: any) => {
          res.status(error.response.status).send(error.response.data);
        });
    }
  );

  app.get('/user/insurances', verifyJWT, async (req: any, res: any) => {
    axios
      .get(`http://${Microservices.user}/insurances`)
      .then((response: any) => {
        res.status(200).send(response.data);
      })
      .catch((error: any) => {
        res.status(error.response.status).send(error.response.data);
      });
  });

  app.get('/user/getAllDoctors/:id_op&:id_est', verifyJWT, async (req: any, res: any) => {
    axios
      .get(`http://${Microservices.user}/getAllDoctors/${req.id_op}&${req.id_est}`)
      .then((response: any) => {
        res.status(200).send(response.data);
      })
      .catch((error: any) => {
        res.status(error.response.status).send(error.response.data);
      });    
  })
};

export default initializeRoutes;
