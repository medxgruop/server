export enum Microservices {
  auth = "10.0.0.1:3001",
  user = "10.0.1.4:3002",
  mobile = "10.0.1.4:3003",
  query = "10.0.0.3:3004",
  mongo = "10.0.1.4:27017"
}
