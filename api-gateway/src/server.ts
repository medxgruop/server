import swaggerDoc from "./middleware/swagger-jsdoc";
import routes from "./routes/routes";
import userRoutes from "./routes/user-routes";
import mobileRoutes from "./routes/mobile-routes";
import schedulerRoutes from "./routes/scheduler-routes";
import { Microservices } from "./urls/url-routes";

require("ts-node").register();
const express = require("express");
const cors = require("cors");
const app = express();
const bodyParser = require("body-parser");
const mysql = require("mysql");
const axios = require("axios");
require("dotenv-safe").load();
const jwt = require("jsonwebtoken");
const port = process.env.PORT || 3000;

function verifyJWT(req: any, res: any, next: any) {
  const authorization = req.headers["authorization"];
  const token = authorization.split("Bearer ")[1];
  console.log(token);
  if (!token)
    return res.status(401).send({ auth: false, message: "No token provided." });

  jwt.verify(token, process.env.SECRET, (err: any, decoded: any) => {
    if (err)
      return res
        .status(500)
        .send({
          auth: false,
          message: "Failed to authenticate token, your token has expired"
        });

    // se tudo estiver ok, salva no request para uso posterior
    req.userLogin = decoded;
    next();
  });
}

let db_config = {
  host: 'medx-database-mysql.mysql.database.azure.com',
  user: 'medxadmin@medx-database-mysql',
  password: 'password@123',
  database: 'medx_database',
  port: 3306,
  ssl: true,
  timeout: 60000,
};

// let connection;
const pool = new mysql.createPool(db_config);

// function handleDisconnect() {

//   connection = new mysql.createConnection(db_config);

//   connection.connect((err:any) => {
//     if (err) { 
//       console.log("!!! Cannot connect !!! Error:");
//       throw err;
//     }
//     else
//     {
//        console.log("Connection established.");
//        if(pool) {
//          //route file
//         }
//       }
//     });
//     connection.on('error',(err:any) => {
//       console.log('db error', err);
//       if(err.code === 'PROTOCOL_CONNECTION_LOST') {
//         handleDisconnect();
//       } else {
//         throw err;
//       }
//     });
//   }
  
//   handleDisconnect();
  
  // parse application/x-www-form-urlencoded
  app.use(bodyParser.urlencoded({ extended: false }));
  
  // parse application/json
  app.use(bodyParser.json());
  
  app.use(cors());
  
  app.listen(port, () =>
  console.log(`Server Api Gateway is running in port: ${port}`)
  );
  
  //swagger docs
  swaggerDoc(app);
  
  //user Routes
  routes(app, jwt, pool);
  userRoutes(app, axios, verifyJWT);
  
//mobile Routes
mobileRoutes(app, axios, verifyJWT, jwt);

//scheduler Routes
schedulerRoutes(app, axios, verifyJWT);
