export interface Adress {
    publicPlace: string;
    numberPlace: number;
    neighborhood: string;
    city: string;
    state: string;
    cep: string;
    complement: string;
}
