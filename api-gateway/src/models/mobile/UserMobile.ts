import { Adress } from "./AdressMobile";

export interface UserMobile {
    id?: number;
    name: string;
    user: string;
    pass: string;
    token: string;
    bithday: string;
    gender: string;
    adress: Adress;
}
