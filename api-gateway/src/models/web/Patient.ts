export interface Patient {
    id?: number;
    name: string;
    tel: string;
    gender: string;
}