export interface Appointment {
    id?: number;
    idSchedule: number;
    startTime: string;
    endTime: string;
    idPatient: string;
}