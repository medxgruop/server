export enum UserType {
    ADMIN = '01',
    DOCTOR = '02',
    OPERATIONS = '03'
}