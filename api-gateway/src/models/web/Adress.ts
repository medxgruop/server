export interface Address {
    id?: number;
    rua: string;
    nomeLocal: string;
    numero: string;
    bairro: string;
    complemento: string;
    cidade: string;
    estado: string;
    cep: string;
}
