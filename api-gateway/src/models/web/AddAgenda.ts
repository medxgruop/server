export interface AddAgenda {
  id?: number;
  days: number;
  profId: number;
  defaultJourneyId: number;
  establishmentId: number;
  hours: string[];
  dom: boolean;
  seg: boolean;
  ter: boolean;
  qua: boolean;
  qui: boolean;
  sex: boolean;
  sab: boolean;
}
