import { UserType } from "./UserType";
import { Specialty } from "./Specialty";
import { OccupationArea } from "./OccupationArea";

export interface UserProfile {
  id?: number;
  name: string;
  username: string;
  pass: string;
  crm: number;
  rqe: number;
  occupationArea: OccupationArea;
  specialty: Specialty;
  cep: string;
  nomeLocal: string;
  numero: string;
  complemento: string;
  rua: string;
  bairro: string;
  cidade: string;
  estado: string;
  accessLevel: UserType;
}
