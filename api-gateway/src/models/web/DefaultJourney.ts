export interface DefaultJourney {
  id?: number;
  idProfh: number;
  idEstablishment: number;
  journeyDate: string;
  startTime: string;
  endTime: string;
  appointmentLenght: number;
  lunchStartTime: string;
  lunchEndTime: string;
}
