export interface MedicalRecord {
    appointmentDate: string;
    startTime: string;
    physicianName: string;
    clinicName: string;
    patientName: string;
    historic: string;
    exames: string;
    examesResults: string;
}