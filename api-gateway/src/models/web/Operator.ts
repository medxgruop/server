import { UserType } from "./UserType";

export interface OperatorReq {
    id?: number;
    name: string;
    function: string;
    username: string;
    password: string;
    userType: UserType;
}

export interface OperatorRes {
    id: number;
    name: string;
    function: string;
    username: string;
    password: string;
    userType: UserType;
}