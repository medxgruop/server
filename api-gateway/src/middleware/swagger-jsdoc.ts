// import * as swaggerJsDoc from 'swagger-jsdoc';
import * as swaggerUi from 'swagger-ui-express';

const swaggerJsDoc = require('swagger-jsdoc');
const basicAuth = require('express-basic-auth');

const swaggerDoc  = (app: any) => {

    const options = {
        swaggerDefinition: {
            info: {
                title: 'MED-X API',
                version: '1.0',
                description: 'RESTful API Services with NodeJS & TypeScript',
            },
            basePath: '/',
            produces: ['application/json'],
            consumes: ['application/json'],
            securityDefinitions: {
              jwt: {
                type: 'apiKey',
                name: 'Authorization',
                in: 'header'
              }
            },
            
        },
        apis:['./src/routes/routes.ts','./src/routes/user-routes.ts',,'./src/routes/mobile-routes.ts']    
    }
    
    const specs = swaggerJsDoc(options);

    const explorerOption = {
        explorer: true
    }

    const userAdmin = {
        username: "admin",
        password: "admin1234"
    }

    const adminSw = true;

    app.use('/swagger', adminSw ? basicAuth({
        users: { [`${userAdmin.username}`]: `${userAdmin.password}` },
        challenge: true,
    }): (req: any, res: any, next: any) => next(), swaggerUi.serve, swaggerUi.setup(specs, explorerOption))
}

export default swaggerDoc