export enum UserType {
    ADMIN = 1,
    DOCTOR = 2,
    OPERATIONS = 3
}