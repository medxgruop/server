import { Adress } from './Adress';
import { Hour } from './Hour';

export interface Scheduler {
    id?: number;
    professionalId: number;
    professionalName: string;
    address: Adress;
    hours: Hour[];
    date: string;
    weekDay: string;
}