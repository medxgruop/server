import { Address } from "../models/Adress";
import { MedicalRecord } from './MedicalRecord';

export interface Patient {
  _id?: string;
  name: string;
  pass: string;
  user: string;
  birthday: string;
  tel: string;
  gender: string;
  cpf: string;
  address: Address;
  insurance: number;
  active: boolean;
  historic?: MedicalRecord[];
}
