import { Address } from "./Adress";

export interface DoctorScheduler {
  prophName: string;
  address: Address;
  times: string[];
  weekDate: number;
}
