import { Prescription } from './Prescription';

export interface MedicalRecord {
    appointmentDate: string;
    startTime: string;
    physicianName: string;
    clinicName: string;
    patientName: string;
    observation: string;
    exames: string;
    examesResults: string;
    prescriptions: Prescription[];
}