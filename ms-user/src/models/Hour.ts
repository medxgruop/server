import { Patient } from './Patient';

export interface Hour {
    startTime: string;
    endTime: string;
    patient?: Patient
}