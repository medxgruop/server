import { Hour } from './Hour';
import { Patient } from './Patient';

export interface TakenSchedule {
    id?: number;
    professionalId: number;
    date: string;
    hour: Hour;
    patient: Patient;
}