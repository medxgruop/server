export interface Permission {
  id: string;
  description: string;
  active: any;
}
