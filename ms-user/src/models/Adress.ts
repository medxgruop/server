export interface Address {
    id?: number;
    publicPlace?: string;
    nomeLocal: string;
    numberPlace: string;
    neighborhood?: string;
    complement: string;
    city?: string;
    state?: string;
    cep?: string;
  }
