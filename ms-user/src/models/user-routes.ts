import { UserProfile } from "../models/UserProfile";
import { Address } from "../models/Adress";
import { Insurance } from "../models/Insurance";

const initializeRoutes = (app, pool) => {
  app.get("/self", async (req, res, next) => {
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);
      let sql = `CALL GET_USER_FULL_INFO('${req.headers.user}')`;

      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(400).send(`Sintaxe error: ${err}`);
        } else {
          if (!result) {
            res.status(400).send(`User not found: ${err}`);
          } else {
            res.status(200).send(JSON.stringify(result[0]));
          }
        }
      });
    });
  });

  app.get("/all", async (req, res, next) => {
    console.log("ALL");
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `CALL GET_ALL_PROFH()`;
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(400).send(`Sintaxe error: ${err}`);
        } else {
          if (!result) {
            res.status(400).send(`User not found: ${err}`);
          } else {
            const users: UserProfile[] = [];
            result[0].map(item => {
              if (!users.find(obj => obj.id == item.ID_PROFH)) {
                users.push({
                  id: item.ID_PROFH,
                  name: item.PROFH_NAME,
                  username: item.USERNAME,
                  pass: item.PASS,
                  userType: item.USER_TYPE,
                  crm: item.CRM,
                  occupationArea: item.COD_AREA,
                  // : item.AREA_DESCRIPTION,
                  specialty: item.COD_ESPC,
                  // ESPC_DESCRIPTION: item.ESPC_DESCRIPTION,
                  rqe: item.RQE,
                  userGroup: item.ID_GROUP,
                  active: item.ACTIVE,
                  address: [
                    {
                      id: item.ID_ESTABLISHMENT,
                      bairro: item.NEIGHBOURHOOD,
                      cep: item.CEP,
                      cidade: item.CITY,
                      complemento: item.ADDITIONAL,
                      estado: item.STATE,
                      nomeLocal: item.ESTP_NAME,
                      numero: item.ADDRESS_NUMBER,
                      rua: item.STREET
                    }
                  ]
                });
              } else {
                users
                  .find(obj => obj.ID_PROFH == item.ID_PROFH)
                  .ADDRESS.push({
                    id: item.ID_ESTABLISHMENT,
                    bairro: item.NEIGHBOURHOOD,
                    cep: item.CEP,
                    cidade: item.CITY,
                    complemento: item.ADDITIONAL,
                    estado: item.STATE,
                    nomeLocal: item.ESTP_NAME,
                    numero: item.ADDRESS_NUMBER,
                    rua: item.STREET
                  });
              }
            });
            console.log(users);
            res.status(200).send(JSON.stringify(users));
          }
        }
      });
    });
  });

  app.post("/add/doctor", async (req, res) => {
    const user: UserProfile = req.body as UserProfile;

    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `SELECT CHECH_USERNAME('${user.username}') as exist`;
      connection.query(sql, true, (err, results, fields) => {
        connection.release();
        if (err) {
          res.status(401).send({ error: "Bad Request" });
        } else {
          if (!results[0].exist) {
            pool.getConnection((err, connection) => {
              if (err) return res.status(400).send(`Sintaxe error: ${err}`);

              let sql = `CALL ADD_PROFH_USER(${user.crm},${user.rqe},"${user.name}",${user.occupationArea},
                ${user.specialty},"${user.username}","${user.pass}",${user.userType},${user.userType}, ${user.active})`;
              connection.query(sql, true, (err, results, fields) => {
                connection.release();
                if (err) {
                  res.status(400).send(`User not added: ${err}`);
                } else {
                  pool.getConnection((err, connection) => {
                    if (err)
                      return res.status(400).send(`Sintaxe error: ${err}`);
                    else {
                      let mySqlSintaxe = "";
                      user.address.map(address => {
                        mySqlSintaxe += `CALL BIND_PROFH_AESTABLISHMENT(${address.id});`;
                        console.log("address bind", address);
                      });
                      connection.query(
                        mySqlSintaxe,
                        true,
                        (err, results, fields) => {
                          connection.release();
                          if (err) {
                            res.status(400).send(`User not added: ${err}`);
                          } else {
                            pool.getConnection((err, connection) => {
                              if (err)
                                return res
                                  .status(400)
                                  .send(`Sintaxe error: ${err}`);
                              let sql = `CALL BIND_USER_GROUP(${user.userType}, '${user.username}');`;
                              connection.query(
                                sql,
                                true,
                                (err, resultsBind, fiendsBind) => {
                                  connection.release();
                                  if (err) {
                                    res
                                      .status(400)
                                      .send(`Bind not effected: ${err}`);
                                  } else {
                                    res
                                      .status(200)
                                      .send({ sucess: "User added" });
                                  }
                                }
                              );
                            });
                          }
                        }
                      );
                    }
                  });
                }
              });
            });
          } else {
            res.status(400).send({ error: "Username already exist" });
          }
        }
      });
    });
  });

  app.post("/add/operational", async (req, res) => {
    const user: UserProfile = req.body as UserProfile;

    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `SELECT CHECH_USERNAME('${user.username}') as exist`;
      connection.query(sql, true, (err, results, fields) => {
        connection.release();
        if (err) {
          res.status(401).send({ error: "Bad Request" });
        } else {
          if (!results[0].exist) {
            pool.getConnection((err, connection) => {
              if (err) return res.status(400).send(`Sintaxe error: ${err}`);

              let sql = `CALL ADD_PROFOP_USER("${user.name}","","${user.username}","${user.pass}",${user.userType},${user.userType}, ${user.active})`;
              connection.query(sql, true, (err, results, fields) => {
                connection.release();
                if (err) {
                  res.status(400).send(`User not added: ${err}`);
                } else {
                  pool.getConnection((err, connection) => {
                    if (err)
                      return res.status(400).send(`Sintaxe error: ${err}`);
                    else {
                      let mySqlSintaxe = "";
                      user.address.map(address => {
                        mySqlSintaxe += `CALL BIND_PROFOPS_AESTABLISHMENT(${address.id});`;
                        console.log("address bind", address);
                      });
                      connection.query(
                        mySqlSintaxe,
                        true,
                        (err, results, fields) => {
                          connection.release();
                          if (err) {
                            res.status(400).send(`User not added: ${err}`);
                          } else {
                            pool.getConnection((err, connection) => {
                              if (err)
                                return res
                                  .status(400)
                                  .send(`Sintaxe error: ${err}`);
                              let sql = `CALL BIND_USER_GROUP(${user.userType}, '${user.username}');`;
                              connection.query(
                                sql,
                                true,
                                (err, resultsBind, fiendsBind) => {
                                  connection.release();
                                  if (err) {
                                    res
                                      .status(400)
                                      .send(`Bind not effected: ${err}`);
                                  } else {
                                    res
                                      .status(200)
                                      .send({ sucess: "User added" });
                                  }
                                }
                              );
                            });
                          }
                        }
                      );
                    }
                  });
                }
              });
            });
          } else {
            res.status(400).send({ error: "Username already exist" });
          }
        }
      });
    });
  });

  app.post("/add/address", async (req, res) => {
    const address: Address = req.body;
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);
      connection.query(
        `SELECT RETURNS_ADDRESS("${address.cep}","${address.nomeLocal}",
        ${address.numero},"${address.complemento}","${address.rua}","${address.bairro}",
        "${address.cidade}","${address.estado}") as exist`,
        true,
        (error, result, fieldss) => {
          connection.release();
          if (error) {
            res.status(400).send(`Adress not exists: ${error}`);
          } else {
            console.log("EXIST ADDRESS: ", result[0].exist);
            if (result[0].exist) {
              let mySqlSintaxe = "";
              if (address) {
                mySqlSintaxe = `CALL ADD_ADDRESS("${address.cep}","${address.nomeLocal}",
                    ${address.numero},"${address.complemento}","${address.rua}","${address.bairro}",
                    "${address.cidade}","${address.estado}");`;
                console.log("address", this.adress);
                pool.getConnection((err, connection) => {
                  if (err) return res.status(400).send(`Sintaxe error: ${err}`);
                  connection.query(
                    mySqlSintaxe,
                    true,
                    (error, result, fieldss) => {
                      connection.release();
                      if (error) {
                        res.status(400).send(`Address not added: ${error}`);
                      } else {
                        res.status(200).send({ success: "Addresses added" });
                      }
                    }
                  );
                });
              }
            } else {
              res.status(401).send({ warning: "Address already exists" });
            }
          }
        }
      );
    });
  });

  app.put("/edit", async (req, res) => {
    const user: UserProfile = req.body as UserProfile;

    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `CALL UPDATE_PROFH_USER(${user.id},${user.crm},${user.rqe},"${user.name}",
      ${user.occupationArea},${user.specialty},"${user.username}","${user.pass}",
      ${user.userType}, ${user.userGroup}, ${user.active})`;

      connection.query(sql, true, (err, results, fields) => {
        connection.release();
        if (err) {
          console.log("ERROR update", err.message);
          res.status(400).send(err.message);
        } else {
          pool.getConnection((err, connection) => {
            if (err) return res.status(400).send(`Sintaxe error: ${err}`);

            user.address.map(adress => {
              let adressAddSql = `CALL UPDATE_ADDRESS(${adress.id},"${adress.cep}","${adress.nomeLocal}",
              ${adress.numero},"${adress.complemento}","${adress.rua}","${adress.bairro}",
              "${adress.cidade}","${adress.estado}")`;

              connection.query(adressAddSql, true, (error, result, fieldss) => {
                connection.release();
                if (error) {
                  res.status(400).send(`User not added: ${error}`);
                } else {
                  res.status(200).send(JSON.stringify(result));
                }
              });
            });
          });
        }
      });
    });
  });

  app.delete("/delete/:id", async (req, res) => {
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `CALL DELETE_PROFH_USER(${req.params.id})`;
      connection.query(sql, true, (err, result, fields) => {
        connection.release();
        if (err) {
          res.status(400).send(`ERROR: ${err}`);
        } else {
          res.status(200).send({ success: true });
        }
      });
    });
  });

  app.get("/occupation", async (req, res) => {
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `CALL RETURN_OCCUPATION_AREA()`;
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(400).send(`User not found: ${err}`);
        } else {
          res.status(200).send(result[0]);
        }
      });
    });
  });

  app.get("/specialty", async (req, res) => {
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `CALL RETURN_ESPECIALTY()`;
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(404).send(`User not found: ${err}`);
        } else {
          res.status(200).send(result[0]);
        }
      });
    });
  });

  app.get("/checkUsername/:user", async (req, res) => {
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `SELECT CHECH_USERNAME('${req.params.user}') as exist`;
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(404).send(`User not found: ${err}`);
        } else {
          if (!result[0].exist) {
            res.status(200).send({ success: true });
          } else {
            res.status(204).send({ error: "User already exist" });
          }
        }
      });
    });
  });

  app.get("/establishments", async (req, res) => {
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `CALL GET_ESTABLISHMENTS()`;
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(404).send(`List of estabilishments not found: ${err}`);
        } else {
          if (result[0]) {
            const addressAll: Address[] = [];

            result[0].map(address => {
              addressAll.push({
                id: address.ID_ESTABLISHMENT,
                bairro: address.NEIGHBOURHOOD,
                cep: address.CEP,
                cidade: address.CITY,
                complemento: address.ADDITIONAL,
                estado: address.STATE,
                nomeLocal: address.ESTP_NAME,
                numero: address.ADDRESS_NUMBER,
                rua: address.STREET
              });
            });
            res.status(200).send(JSON.stringify(addressAll));
          }
        }
      });
    });
  });
  app.get("/proph/establishments/:id", async (req, res) => {
    const id = req.params.id;
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `CALL GET_PROFH_ADDRESS(${id})`;
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(404).send(`List of estabilishments not found: ${err}`);
        } else {
          if (result[0]) {
            const addressAll: Address[] = [];

            result[0].map(address => {
              addressAll.push({
                id: address.ID_ESTABLISHMENT,
                bairro: address.NEIGHBOURHOOD,
                cep: address.CEP,
                cidade: address.CITY,
                complemento: address.ADDITIONAL,
                estado: address.STATE,
                nomeLocal: address.ESTP_NAME,
                numero: address.ADDRESS_NUMBER,
                rua: address.STREET
              });
            });
            res.status(200).send(JSON.stringify(addressAll));
          }
        }
      });
    });
  });
  app.get("/establishments/proph/:id", async (req, res) => {
    const id = req.params.id;
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `CALL GET_ADDRESS_PROFH(${id})`;
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(404).send(`List of estabilishments not found: ${err}`);
        } else {
          if (result[0]) {
            const establishmentsDoctor = [];
            result[0].map(item => {
              const address: Address = {
                id: item.ID_ESTABLISHMENT,
                bairro: item.NEIGHBOURHOOD,
                cep: item.CEP,
                cidade: item.CITY,
                complemento: item.ADDITIONAL,
                estado: item.STATE,
                nomeLocal: item.ESTP_NAME,
                numero: item.ADDRESS_NUMBER,
                rua: item.STREET
              };
              establishmentsDoctor.push({
                id: item.ID_PROFH,
                proph_name: item.PROFH_NAME,
                address: address
              });
            });
            res.status(200).send(JSON.stringify(establishmentsDoctor));
          }
        }
      });
    });
  });
  app.get("/insurances", async (req, res) => {
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `CALL GET_INSURANCE()`;
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(404).send(`User not found: ${err}`);
        } else {
          if (result[0]) {
            const insurances: Insurance[] = result[0].map(item => {
              const insurance: Insurance = {
                id: item.COD_INSURANCE,
                name: item.INSURANCE_NAME
              };
              return insurance;
            });
            res.status(200).send(insurances);
          }
        }
      });
    });
  });
};

export default initializeRoutes;
