import { Permission } from "./Permission";

export interface Group {
    id?: number,
    name: string,
    description: string,
    active: boolean,
    arrayPermissions: Permission[],
    arrayIDUser: number[]
}