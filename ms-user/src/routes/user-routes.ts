import { UserProfile } from "../models/UserProfile";
import { Address } from "../models/Adress";
import { Insurance } from "../models/Insurance";
import { OccupationArea } from "../models/OccupationArea";
import { Specialty } from "../models/Specialty";

const initializeRoutes = (app, pool) => {
  app.get("/self", async (req, res, next) => {
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);
      let sql = `CALL GET_USER_FULL_INFO('${req.headers.user}')`;
      console.log(sql);

      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(400).send(`Sintaxe error: ${err}`);
        } else {
          if (!result) {
            res.status(400).send(`User not found: ${err}`);
          } else {
            const users: UserProfile[] = [];
            result[0].map(item => {
              if (!users.find(obj => obj.id == item.ID_PROFH)) {
                const occupationArea: OccupationArea = {
                  COD_AREA: item.FK_COD_OCCUPATION_AREA,
                  AREA_DESCRIPTION: item.AREA_DESCRIPTION
                };
                const specialty: Specialty = {
                  COD_ESPC: item.FK_COD_ESPECIALTY,
                  ESPC_DESCRIPTION: item.ESPC_DESCRIPTION
                };
                users.push({
                  id: item.ID_PROFH ? item.ID_PROFH : item.ID_PROFOPS,
                  name: item.PROFH_NAME ? item.PROFH_NAME : item.PROFOPS_NAME,
                  username: item.USERNAME ? item.USERNAME : item.FK_USERNAME,
                  pass: item.PASS,
                  userType: item.USER_TYPE,
                  crm: item.CRM ? item.CRM : undefined,
                  occupationArea: occupationArea ? occupationArea : undefined,
                  specialty: specialty ? specialty : undefined,
                  rqe: item.RQE ? item.RQE : undefined,
                  responsability: item.PROFOPS_FUNCTION
                    ? item.PROFOPS_FUNCTION
                    : undefined,
                  userGroup: item.ID_GROUP,
                  active: item.ACTIVE,
                  address: [
                    {
                      id: item.ID_ESTABLISHMENT,
                      neighborhood: item.NEIGHBOURHOOD,
                      cep: item.CEP,
                      city: item.CITY,
                      complement: item.ADDITIONAL,
                      state: item.STATE,
                      nomeLocal: item.ESTP_NAME,
                      numberPlace: item.ADDRESS_NUMBER,
                      publicPlace: item.STREET
                    }
                  ]
                });
              } else {
                users
                  .find(obj => obj.id == item.ID_PROFH)
                  .address.push({
                    id: item.ID_ESTABLISHMENT,
                    neighborhood: item.NEIGHBOURHOOD,
                    cep: item.CEP,
                    city: item.CITY,
                    complement: item.ADDITIONAL,
                    state: item.STATE,
                    nomeLocal: item.ESTP_NAME,
                    numberPlace: item.ADDRESS_NUMBER,
                    publicPlace: item.STREET
                  });
              }
            });
            // if (result[0].ID_PROFH) {
            //   const occupationArea: OccupationArea = {
            //     COD_AREA: result[0].COD_AREA,
            //     AREA_DESCRIPTION: result[0].AREA_DESCRIPTION
            //   };
            //   const specialty: Specialty = {
            //     COD_ESPC: result[0].COD_ESPC,
            //     ESPC_DESCRIPTION: result[0].ESPC_DESCRIPTION
            //   };
            //   result[0].ADDRESS.map(address => {
            //     addresses.push({
            //       id: address.ID_ESTABLISHMENT,
            //       bairro: address.NEIGHBOURHOOD,
            //       cep: address.CEP,
            //       cidade: address.CITY,
            //       complemento: address.ADDITIONAL,
            //       estado: address.STATE,
            //       nomeLocal: address.ESTP_NAME,
            //       numero: address.ADDRESS_NUMBER,
            //       rua: address.STREET
            //     });
            //   });
            //   user = {
            //     id: result[0].ID_PROFH,
            //     name: result[0].PROFH_NAME,
            //     username: result[0].USERNAME,
            //     pass: result[0].PASS,
            //     userType: result[0].USER_TYPE,
            //     crm: result[0].CRM,
            //     occupationArea: occupationArea,
            //     specialty: specialty,
            //     rqe: result[0].RQE,
            //     userGroup: result[0].ID_GROUP,
            //     active: result[0].ACTIVE,
            //     address: addresses
            //   };
            // } else {
            //   result[0].ADDRESS.map(address => {
            //     addresses.push({
            //       id: result[0].ID_ESTABLISHMENT,
            //       bairro: result[0].NEIGHBOURHOOD,
            //       cep: result[0].CEP,
            //       cidade: result[0].CITY,
            //       complemento: result[0].ADDITIONAL,
            //       estado: result[0].STATE,
            //       nomeLocal: result[0].ESTP_NAME,
            //       numero: result[0].ADDRESS_NUMBER,
            //       rua: result[0].STREET
            //     });
            //   });
            //   user = {
            //     id: result[0].ID_PROFOPS,
            //     name: result[0].PROFOPS_NAME,
            //     username: result[0].FK_USERNAME,
            //     pass: result[0].PASS,
            //     userType: result[0].USER_TYPE,
            //     crm: undefined,
            //     occupationArea: undefined,
            //     specialty: undefined,
            //     rqe: undefined,
            //     userGroup: result[0].ID_GROUP,
            //     active: result[0].ACTIVE,
            //     address: addresses
            //   };
            // }
            console.log(users);
            res.status(200).send(JSON.stringify(users));
          }
        }
      });
    });
  });

  app.get("/all", async (req, res, next) => {
    console.log("ALL");
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `CALL GET_ALL_PROFH();CALL GET_ALL_PROFOPS()`;
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(400).send(`Sintaxe error: ${err}`);
        } else {
          if (!result) {
            res.status(400).send(`User not found: ${err}`);
          } else {
            const users = [];
            const userProfh: UserProfile[] = [];
            const userProfops: UserProfile[] = [];
            result[0].map(item => {
              if (!userProfh.find(obj => obj.id == item.ID_PROFH)) {
                const occupationArea: OccupationArea = {
                  COD_AREA: item.COD_AREA,
                  AREA_DESCRIPTION: item.AREA_DESCRIPTION
                };
                const specialty: Specialty = {
                  COD_ESPC: item.COD_ESPC,
                  ESPC_DESCRIPTION: item.ESPC_DESCRIPTION
                };
                userProfh.push({
                  id: item.ID_PROFH,
                  name: item.PROFH_NAME,
                  username: item.USERNAME,
                  pass: item.PASS,
                  userType: item.USER_TYPE,
                  crm: item.CRM,
                  occupationArea: occupationArea,
                  specialty: specialty,
                  rqe: item.RQE,
                  userGroup: item.ID_GROUP,
                  active: item.ACTIVE,
                  address: [
                    {
                      id: item.ID_ESTABLISHMENT,
                      neighborhood: item.NEIGHBOURHOOD,
                      cep: item.CEP,
                      city: item.CITY,
                      complement: item.ADDITIONAL,
                      state: item.STATE,
                      nomeLocal: item.ESTP_NAME,
                      numberPlace: item.ADDRESS_NUMBER,
                      publicPlace: item.STREET
                    }
                  ]
                });
              } else {
                userProfh
                  .find(obj => obj.id == item.ID_PROFH)
                  .address.push({
                    id: item.ID_ESTABLISHMENT,
                    neighborhood: item.NEIGHBOURHOOD,
                    cep: item.CEP,
                    city: item.CITY,
                    complement: item.ADDITIONAL,
                    state: item.STATE,
                    nomeLocal: item.ESTP_NAME,
                    numberPlace: item.ADDRESS_NUMBER,
                    publicPlace: item.STREET
                  });
              }
            });
            users.push(userProfh);
            result[2].map(item => {
              if (!userProfops.find(obj => obj.id == item.ID_PROFOPS)) {
                userProfops.push({
                  id: item.ID_PROFOPS,
                  name: item.PROFOPS_NAME,
                  username: item.FK_USERNAME,
                  pass: item.PASS,
                  userType: item.USER_TYPE,
                  crm: undefined,
                  occupationArea: undefined,
                  specialty: undefined,
                  rqe: undefined,
                  userGroup: undefined,
                  active: undefined,
                  responsability: item.PROFOPS_FUNCTION,
                  address: [
                    {
                      id: item.ID_ESTABLISHMENT,
                      neighborhood: item.NEIGHBOURHOOD,
                      cep: item.CEP,
                      city: item.CITY,
                      complement: item.ADDITIONAL,
                      state: item.STATE,
                      nomeLocal: item.ESTP_NAME,
                      numberPlace: item.ADDRESS_NUMBER,
                      publicPlace: item.STREET
                    }
                  ]
                });
              } else {
                userProfops
                  .find(obj => obj.id == item.ID_PROFOPS)
                  .address.push({
                    id: item.ID_ESTABLISHMENT,
                    neighborhood: item.NEIGHBOURHOOD,
                    cep: item.CEP,
                    city: item.CITY,
                    complement: item.ADDITIONAL,
                    state: item.STATE,
                    nomeLocal: item.ESTP_NAME,
                    numberPlace: item.ADDRESS_NUMBER,
                    publicPlace: item.STREET
                  });
              }
            });
            users.push(userProfops);
            console.log(users);
            res.status(200).send(users);
          }
        }
      });
    });
  });

  app.post("/add/doctor", async (req, res) => {
    const user: UserProfile = req.body as UserProfile;

    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `SELECT CHECH_USERNAME('${user.username}') as exist`;
      connection.query(sql, true, (err, results, fields) => {
        connection.release();
        if (err) {
          res.status(401).send({ error: "Bad Request" });
        } else {
          if (!results[0].exist) {
            pool.getConnection((err, connection) => {
              if (err) return res.status(400).send(`Sintaxe error: ${err}`);

              let sql = `CALL ADD_PROFH_USER(${user.crm},${user.rqe},"${user.name}",${user.occupationArea},
                ${user.specialty},"${user.username}","${user.pass}",${user.userType},${user.userType}, ${user.active})`;
              connection.query(sql, true, (err, results, fields) => {
                connection.release();
                if (err) {
                  res.status(400).send(`User not added: ${err}`);
                } else {
                  pool.getConnection((err, connection) => {
                    if (err)
                      return res.status(400).send(`Sintaxe error: ${err}`);
                    else {
                      let mySqlSintaxe = "";
                      user.address.map(address => {
                        mySqlSintaxe += `CALL BIND_PROFH_AESTABLISHMENT(${address.id});`;
                        console.log("address bind", address);
                      });
                      connection.query(
                        mySqlSintaxe,
                        true,
                        (err, results, fields) => {
                          connection.release();
                          if (err) {
                            res.status(400).send(`User not added: ${err}`);
                          } else {
                            pool.getConnection((err, connection) => {
                              if (err)
                                return res
                                  .status(400)
                                  .send(`Sintaxe error: ${err}`);
                              let sql = `CALL BIND_USER_GROUP(${user.userType}, '${user.username}');`;
                              connection.query(
                                sql,
                                true,
                                (err, resultsBind, fiendsBind) => {
                                  connection.release();
                                  if (err) {
                                    res
                                      .status(400)
                                      .send(`Bind not effected: ${err}`);
                                  } else {
                                    res
                                      .status(200)
                                      .send({ sucess: "User added" });
                                  }
                                }
                              );
                            });
                          }
                        }
                      );
                    }
                  });
                }
              });
            });
          } else {
            res.status(400).send({ error: "Username already exist" });
          }
        }
      });
    });
  });

  app.post("/add/operational", async (req, res) => {
    const user: UserProfile = req.body as UserProfile;

    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `SELECT CHECH_USERNAME('${user.username}') as exist`;
      connection.query(sql, true, (err, results, fields) => {
        connection.release();
        if (err) {
          res.status(401).send({ error: "Bad Request" });
        } else {
          if (!results[0].exist) {
            pool.getConnection((err, connection) => {
              if (err) return res.status(400).send(`Sintaxe error: ${err}`);

              let sql = `CALL ADD_PROFOP_USER("${user.name}","","${user.username}","${user.pass}",${user.userType},${user.userType}, ${user.active})`;
              connection.query(sql, true, (err, results, fields) => {
                connection.release();
                if (err) {
                  res.status(400).send(`User not added: ${err}`);
                } else {
                  pool.getConnection((err, connection) => {
                    if (err)
                      return res.status(400).send(`Sintaxe error: ${err}`);
                    else {
                      let mySqlSintaxe = "";
                      user.address.map(address => {
                        mySqlSintaxe += `CALL BIND_PROFOPS_AESTABLISHMENT(${address.id});`;
                        console.log("address bind", address);
                      });
                      connection.query(
                        mySqlSintaxe,
                        true,
                        (err, results, fields) => {
                          connection.release();
                          if (err) {
                            res.status(400).send(`User not added: ${err}`);
                          } else {
                            pool.getConnection((err, connection) => {
                              if (err)
                                return res
                                  .status(400)
                                  .send(`Sintaxe error: ${err}`);
                              let sql = `CALL BIND_USER_GROUP(${user.userType}, '${user.username}');`;
                              connection.query(
                                sql,
                                true,
                                (err, resultsBind, fiendsBind) => {
                                  connection.release();
                                  if (err) {
                                    res
                                      .status(400)
                                      .send(`Bind not effected: ${err}`);
                                  } else {
                                    res
                                      .status(200)
                                      .send({ sucess: "User added" });
                                  }
                                }
                              );
                            });
                          }
                        }
                      );
                    }
                  });
                }
              });
            });
          } else {
            res.status(400).send({ error: "Username already exist" });
          }
        }
      });
    });
  });

  app.post("/add/address", async (req, res) => {
    const address: Address = req.body;
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);
      connection.query(
        `SELECT RETURNS_ADDRESS("${address.cep}","${address.nomeLocal}",
        ${address.numberPlace},"${address.complement}","${address.publicPlace}","${address.neighborhood}",
        "${address.city}","${address.state}") as exist`,
        true,
        (error, result, fieldss) => {
          connection.release();
          if (error) {
            res.status(400).send(`Adress not exists: ${error}`);
          } else {
            console.log("EXIST ADDRESS: ", result[0].exist);
            if (result[0].exist) {
              let mySqlSintaxe = "";
              if (address) {
                mySqlSintaxe = `CALL ADD_ADDRESS("${address.cep}","${address.nomeLocal}",
                    ${address.numberPlace},"${address.complement}","${address.publicPlace}","${address.neighborhood}",
                    "${address.city}","${address.state}");`;
                console.log("address", this.adress);
                pool.getConnection((err, connection) => {
                  if (err) return res.status(400).send(`Sintaxe error: ${err}`);
                  connection.query(
                    mySqlSintaxe,
                    true,
                    (error, result, fieldss) => {
                      connection.release();
                      if (error) {
                        res.status(400).send(`Address not added: ${error}`);
                      } else {
                        res.status(200).send({ success: "Addresses added" });
                      }
                    }
                  );
                });
              }
            } else {
              res.status(401).send({ warning: "Address already exists" });
            }
          }
        }
      );
    });
  });

  app.put("/edit", async (req, res) => {
    const user: UserProfile = req.body as UserProfile;

    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);
      let sql;
      if(user.userType == 2) {
        sql = `CALL UPDATE_PROFH_USER(${user.id},${user.crm},${user.rqe},"${user.name}",
        ${user.occupationArea.COD_AREA},${user.specialty.COD_ESPC},"${user.username}","${user.pass}",
        ${user.userType}, ${user.userGroup}, ${user.active})`;
        console.log(sql);
      }
      else {
        sql = `CALL UPDATE_PROFOP_USER(${user.id},"${user.name}","${user.responsability}","${user.username}","${user.pass}",${user.userType},${user.active})`;
        console.log(sql);
      }

      connection.query(sql, true, (err, results, fields) => {
        connection.release();
        if (err) {
          console.log("ERROR update", err.message);
          res.status(400).send(err.message);
        } else {
          pool.getConnection(async (err, connection) => {
            if (err) return res.status(400).send(`Sintaxe error: ${err}`);
            let sqlAddress = '';
            user.address.forEach(adress => {
              sqlAddress += `CALL UPDATE_ADDRESS(${adress.id},"${adress.cep}","${adress.nomeLocal}",${adress.numberPlace},"${adress.complement}","${adress.publicPlace}","${adress.neighborhood}","${adress.city}","${adress.state}");`;          
            });
            await connection.query(sqlAddress, true, (error, result, fieldss) => {
              connection.release();
              if (error) {
                console.log("ERROR", error);
                res.status(400).send(`User not added: ${error}`);
              } else {
                console.log("SUCESSO");
                res.status(200).send(JSON.stringify(result));
              }
            }); 
          });
        }
      });
    });
  });

  app.delete("/delete/:id&:active&:type", async (req, res) => {
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);
      let sql;
      if(req.params.type == 2) {
        sql = `CALL ALTER_ACTIVE_PROFH_USER(${req.params.id},'${req.params.active}')`;
      }
      else {
        sql = `CALL ALTER_ACTIVE_PROFOP_USER(${req.params.id},'${req.params.active}')`;
      }

      console.log(sql);

      connection.query(sql, true, (err, result, fields) => {
        connection.release();
        if (err) {
          res.status(400).send(`ERROR: ${err}`);
        } else {
          res.status(200).send({ success: true });
        }
      });
    });
  });

  app.get("/occupation", async (req, res) => {
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `CALL RETURN_OCCUPATION_AREA()`;
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(400).send(`User not found: ${err}`);
        } else {
          res.status(200).send(result[0]);
        }
      });
    });
  });

  app.get("/specialty", async (req, res) => {
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `CALL RETURN_ESPECIALTY()`;
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(404).send(`User not found: ${err}`);
        } else {
          res.status(200).send(result[0]);
        }
      });
    });
  });

  app.get("/checkUsername/:user", async (req, res) => {
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `SELECT CHECH_USERNAME('${req.params.user}') as exist`;
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(404).send(`User not found: ${err}`);
        } else {
          if (!result[0].exist) {
            res.status(200).send({ success: true });
          } else {
            res.status(204).send({ error: "User already exist" });
          }
        }
      });
    });
  });

  app.get("/establishments", async (req, res) => {
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `CALL GET_ESTABLISHMENTS()`;
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(404).send(`List of estabilishments not found: ${err}`);
        } else {
          if (result[0]) {
            const addressAll: Address[] = [];

            result[0].map(address => {
              addressAll.push({
                id: address.ID_ESTABLISHMENT,
                neighborhood: address.NEIGHBOURHOOD,
                cep: address.CEP,
                city: address.CITY,
                complement: address.ADDITIONAL,
                state: address.STATE,
                nomeLocal: address.ESTP_NAME,
                numberPlace: address.ADDRESS_NUMBER,
                publicPlace: address.STREET
              });
            });
            res.status(200).send(JSON.stringify(addressAll));
          }
        }
      });
    });
  });
  app.get("/proph/establishments/:id", async (req, res) => {
    const id = req.params.id;
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `CALL GET_PROFH_ADDRESS(${id})`;
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(404).send(`List of estabilishments not found: ${err}`);
        } else {
          if (result[0]) {
            const addressAll: Address[] = [];

            result[0].map(address => {
              addressAll.push({
                id: address.ID_ESTABLISHMENT,
                neighborhood: address.NEIGHBOURHOOD,
                cep: address.CEP,
                city: address.CITY,
                complement: address.ADDITIONAL,
                state: address.STATE,
                nomeLocal: address.ESTP_NAME,
                numberPlace: address.ADDRESS_NUMBER,
                publicPlace: address.STREET
              });
            });
            res.status(200).send(JSON.stringify(addressAll));
          }
        }
      });
    });
  });
  app.get("/establishments/proph/:id", async (req, res) => {
    const id = req.params.id;
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `CALL GET_ADDRESS_PROFH(${id})`;
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(404).send(`List of estabilishments not found: ${err}`);
        } else {
          if (result[0]) {
            const establishmentsDoctor = [];
            result[0].map(item => {
              const address: Address = {
                id: item.ID_ESTABLISHMENT,
                neighborhood: item.NEIGHBOURHOOD,
                cep: item.CEP,
                city: item.CITY,
                complement: item.ADDITIONAL,
                state: item.STATE,
                nomeLocal: item.ESTP_NAME,
                numberPlace: item.ADDRESS_NUMBER,
                publicPlace: item.STREET
              };
              establishmentsDoctor.push({
                id: item.ID_PROFH,
                proph_name: item.PROFH_NAME,
                address: address
              });
            });
            res.status(200).send(JSON.stringify(establishmentsDoctor));
          }
        }
      });
    });
  });

  app.get("/profops/establishments/:id", async (req, res) => {
    const id = req.params.id;
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `CALL GET_PROFOPS_ADDRESS(${id})`;
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(404).send(`List of estabilishments not found: ${err}`);
        } else {
          if (result[0]) {
            const addressAll: Address[] = [];

            result[0].map(address => {
              addressAll.push({
                id: address.ID_ESTABLISHMENT,
                neighborhood: address.NEIGHBOURHOOD,
                cep: address.CEP,
                city: address.CITY,
                complement: address.ADDITIONAL,
                state: address.STATE,
                nomeLocal: address.ESTP_NAME,
                numberPlace: address.ADDRESS_NUMBER,
                publicPlace: address.STREET
              });
            });
            res.status(200).send(JSON.stringify(addressAll));
          }
        }
      });
    });
  });
  app.get("/establishments/profops/:id", async (req, res) => {
    const id = req.params.id;
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `CALL GET_ADDRESS_PROFOPS(${id})`;
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(404).send(`List of estabilishments not found: ${err}`);
        } else {
          if (result[0]) {
            const establishmentsProfops = [];
            result[0].map(item => {
              const address: Address = {
                id: item.ID_ESTABLISHMENT,
                neighborhood: item.NEIGHBOURHOOD,
                cep: item.CEP,
                city: item.CITY,
                complement: item.ADDITIONAL,
                state: item.STATE,
                nomeLocal: item.ESTP_NAME,
                numberPlace: item.ADDRESS_NUMBER,
                publicPlace: item.STREET
              };
              establishmentsProfops.push({
                id: item.ID_PROFOPS,
                proph_name: item.PROFOPS_NAME,
                address: address
              });
            });
            res.status(200).send(JSON.stringify(establishmentsProfops));
          }
        }
      });
    });
  });
  app.get("/insurances", async (req, res) => {
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `CALL GET_INSURANCE()`;
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(404).send(`User not found: ${err}`);
        } else {
          if (result[0]) {
            const insurances: Insurance[] = result[0].map(item => {
              const insurance: Insurance = {
                id: item.COD_INSURANCE,
                name: item.INSURANCE_NAME
              };
              return insurance;
            });
            res.status(200).send(insurances);
          }
        }
      });
    });
  });
};

export default initializeRoutes;
