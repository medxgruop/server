import { AddAgenda } from '../models/AddAgenda';
import { DefaultJourney } from '../models/DefaultJourney';
import { Scheduler } from '../models/SchedulerAppointment';
import { Hour } from '../models/Hour';
import { Patient } from '../models/Patient';
import { Appointment } from '../models/Appointment';
import { TakenSchedule } from '../models/TakenSchedule';
import { resolve } from 'dns';
import { rejects } from 'assert';

const initializeRoutes = (app, pool, mongoClient, mongo, URL_SERVER) => {
  /** SCHEDULER */

    const url = `mongodb://medx-mongodb:5flGVGPwpMO9yX4KsmT9xO7aHGoHvM7qsh4EmWXZ9ppm40Mmh4rYUG9e64gw9wNRO7doQAqxk2qt7dpbdsC4OA==@${URL_SERVER}/?ssl=true&replicaSet=globaldb`;
    const ObjectId = mongo.ObjectID;

  app.post('/scheduler/add', async (req, res) => {
    const scheduler: AddAgenda = req.body;
    const times: string[] = [];

    if (scheduler.hours.length > 0) {
      scheduler.hours.map((item, index) => {
        times.push(item);
      });
      const lengthTimes = times.length;

      if (lengthTimes < 48) {
        for (let index = 0; index < 48 - lengthTimes; index++) {
          times.push('');
        }
      }
    }
    console.log('ADD SCHEDULER', scheduler);
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `CALL ADD_SCHEDULE(${scheduler.days}, ${scheduler.profId}, ${scheduler.defaultJourneyId}, ${scheduler.establishmentId},
        '${times[0]}','${times[1]}','${times[2]}','${times[3]}','${times[4]}','${times[5]}','${times[6]}','${times[7]}',
        '${times[8]}','${times[9]}','${times[10]}','${times[11]}','${times[12]}','${times[13]}','${times[14]}','${times[15]}',
        '${times[16]}','${times[17]}','${times[18]}','${times[19]}','${times[20]}','${times[21]}','${times[22]}','${times[23]}',
        '${times[24]}','${times[25]}','${times[26]}','${times[27]}','${times[28]}','${times[29]}','${times[30]}','${times[31]}',
        '${times[32]}','${times[33]}','${times[34]}','${times[35]}','${times[36]}','${times[37]}','${times[38]}','${times[39]}',
        '${times[40]}','${times[41]}','${times[42]}','${times[43]}','${times[44]}','${times[45]}','${times[46]}','${times[47]}',
        ${scheduler.dom}, ${scheduler.seg}, ${scheduler.ter},${scheduler.qua}, ${scheduler.qui}, ${scheduler.sex}, ${scheduler.sab})`;
        console.log(sql);
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(404).send(`AddAgenda not added: ${err}`);
        } else {
          res.status(200).send({ success: true });
        }
      });
    });
  });

  app.post('/scheduler/default/add', async (req, res) => {
    const journey: DefaultJourney = req.body;

    console.log('JOURNEY USER', journey);
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);
      let sql = `CALL ADD_DEFAULT_JOURNEY(${journey.idProfh},${journey.idEstablishment},"${journey.journeyDate}","${journey.startTime}",
      "${journey.endTime}",${journey.appointmentLenght},"${journey.lunchStartTime}","${journey.lunchEndTime}")`;
      console.log(sql);
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(404).send(`Default Journey not added: ${err}`);
        } else {
          res.status(200).send({ id: result[0][0].id });
        }
      });
    });
  });

  app.get('/scheduler/doctor/:id', async (req, res) => {
    const id = req.params.id;
    await pool.getConnection(async (err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `CALL GET_PROFH_SCHEDULE_MOBILE(${id}); CALL GET_PROFH_SCHEDULE(${id})`;
      await connection.query(sql, true, async (err, result, field) => {
        connection.release();
        if (err) {
          res.status(400).send(`Proph Schedules not Found: ${err}`);
        } 
        // res.status(200).send(result);
        else {
          const results = result[0];
          const resultsWithouCode = result[2];
          let schedulers: Scheduler[] = [];
          for (let index = 0; index < results.length; index++) {
            const item = results[index];
            const itemWithoutCode = resultsWithouCode[index];
            const hoursReturn: Hour[] = [];

            for (let i = 1; i <= 24; i++) {
              const clausule = i <=9 ? "0"+i : i;
              const clausule2 = (i+1) <=9 ? "0"+(i+1) : (i+1);
              const startTime = item["FLEX_TIME"+ clausule +""];
              const endTime = item["FLEX_TIME"+ clausule2 +""];
              const startTimeWithoutCode = itemWithoutCode["FLEX_TIME"+ clausule +""];
              const endTimeWithoutCode = itemWithoutCode["FLEX_TIME"+ clausule2 +""];
              if (startTime !== '' && endTime !== '') {
                
                if(startTime === endTime) {
                    const patientReturn = await getPatientMongo(startTime);
                    hoursReturn.push({
                      startTime: startTimeWithoutCode,
                      endTime: endTimeWithoutCode,
                      patient: patientReturn
                    })                  
                }
                else {
                  hoursReturn.push({
                    startTime,
                    endTime,
                  })
                }
              }
              else {}
              i++;
            }
            let scheduler = {
              id: item.ID,
              professionalId: item.ID_PROFH,
              professionalName: item.PROFH_NAME,
              date: item.DATA_SCH,
              weekDay: item.FK_WEEK_DAY,
              address: {
                id: item.ID_ESTABLISHMENT,
                cep: item.CEP,
                city: item.CITY,
                complement: item.ADDITIONAL,
                neighborhood: item.NEIGHBOURHOOD,
                numberPlace: item.ADDRESS_NUMBER,
                publicPlace: item.ESTP_NAME,
                state: item.STATE
              },
              hours: hoursReturn
            }
            schedulers.push(scheduler);
          }
          res.status(200).send(schedulers);
        }
      });
    });
  });

  const getPatientMongo = async (patient: string): Promise<Patient> => {
    try {
        const client = await mongoClient.connect(url);
        console.log("Connected successfully to server");
        const db = client.db('medx')
        const collection = db.collection('User')
        const cursor = await collection.find({"_id": new ObjectId(`${patient}`)});
        for (let doc = await cursor.next(); doc != null; doc = await cursor.next()) {
          console.log(doc.name);
          return doc;
        }
    } catch (error) {
      console.log('error')
      return
    }
  }

  app.get('/scheduler/default/isEnable/:id&:id_est', async (req, res) => {
    const id = req.params.id;
    const id_est = req.params.id_est;
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `SELECT RETURN_DEFAULT_JOURNEY(${id}, ${id_est}) as enable`;
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(400).send(`Proph Schedules not Found: ${err}`);
        } else {
          
          res.status(200).send(JSON.stringify({ "isEnable": result[0].enable == 0 ? false : true }));
        }
      });
    });
  });

  /**############## APPOINTMENT */

  app.post('/appointment/add', async (req: any, res: any) => {
    const appointment: Appointment = req.body;
    console.log('Appointment', appointment);
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);
      let sql = `CALL MAKE_APPOINTMENT(${appointment.idSchedule},"${appointment.startTime}",
      "${appointment.endTime}","${appointment.idPatient}")`;
      console.log(sql);
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(404).send(`Appointment not added: ${err}`);
        } else {
          res.status(201).send({ success: true });
        }
      });
    });    
  });

  app.delete('/appointment/delete/:id_sched&:id_patient', async (req: any, res: any) => {
    const id_sched = req.params.id_sched;
    const id_patient = req.params.id_patient;
    pool.getConnection((err, connection) => {
      if (err) return res.status(400).send(`Sintaxe error: ${err}`);

      let sql = `CALL CANCEL_APPOINTMENT(${id_sched}, "${id_patient}")`;
      connection.query(sql, true, (err, result, field) => {
        connection.release();
        if (err) {
          res.status(400).send(`Appointment not Found: ${err}`);
        } else {
          
          res.status(200).send({success: true});
        }
      });
    });
  });
};

export default initializeRoutes;
