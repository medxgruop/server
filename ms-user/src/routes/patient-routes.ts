import { Patient } from "../models/Patient";
import { Address } from "../models/Adress";
import { MedicalRecord } from '../models/MedicalRecord';

const initializeRoutes = (app, pool, mongoClient, mongo, URL_SERVER) => {

  const url = `mongodb://medx-mongodb:5flGVGPwpMO9yX4KsmT9xO7aHGoHvM7qsh4EmWXZ9ppm40Mmh4rYUG9e64gw9wNRO7doQAqxk2qt7dpbdsC4OA==@${URL_SERVER}/?ssl=true&replicaSet=globaldb`;
  const ObjectId = mongo.ObjectID;

  app.get("/patients", async (req, res) => {
    console.log("ALL");
    mongoClient.connect(url, (err, client) => {
      if (err) {
        console.error(err)
        return
      }
      console.log("Connected successfully to server");
      const db = client.db('medx')
      const collection = db.collection('User')
      collection.find().toArray((err, items) => {
          if(err) {
              client.close();
              res.status(400).send(JSON.stringify({error: err}));
          }

          client.close();
          console.log("PATIENTS: ", JSON.stringify(items));
          res.status(200).send(items);
      });
  })
  });

  app.get("/patient/historic/:id", async (req, res) => {
    const id = req.params.id;
    console.log("ALL HISTORIC");
    mongoClient.connect(url, (err, client) => {
      if (err) {
        console.error(err)
        return
      }
      console.log("Connected successfully to server");
      const db = client.db('medx')
      const collection = db.collection('User')
      collection.find({"_id" : new ObjectId(id)}, { projection: {historic: 1, _id: 0}}).toArray((err, items) => {
          if(err) {
              client.close();
              res.status(400).send(JSON.stringify({error: err}));
          }

          client.close();
          console.log("PATIENTS: ", JSON.stringify(items[0].historic));
          res.status(200).send(items[0].historic);
      });
  })
  });

  app.post("/patient/add", async (req, res, next) => {
    try {
      const patient: Patient = req.body;
      console.log("ADD", patient);
      mongoClient.connect(url, (err, client) => {
        if (err) {
          console.error(err)
          return
        }
        console.log("Connected successfully to server");
        const db = client.db('medx')
        const collection = db.collection('User')
        collection.insertOne(patient, (err, result) => {
            if(err) {
                client.close();
                res.status(400).send(JSON.stringify({error: err}));
            }

            client.close();
            res.status(200).send(JSON.stringify({success: result}));
        });
    })
    } catch (err) {
      next(err);
    }
  });

  app.put("/patient/edit", async (req, res, next) => {
    try {
      const patient: Patient = req.body;
      const id = patient._id;
      console.log("UPDATE", patient);
      mongoClient.connect(url, (err, client) => {
        if (err) {
            console.error(err)
            return
        }
        console.log("Connected successfully to server");
        const db = client.db('medx')
        const collection = db.collection('User')
        console.log({"_id": id});
        collection.update({"_id": new ObjectId(id)}, {$set: {
          "name": patient.name,
          "pass": patient.pass,
          "user": patient.user,
          "birthday": patient.birthday,
          "tel": patient.tel,
          "gender": patient.gender,
          "cpf": patient.cpf,
          "address": patient.address,
          "insurance": patient.insurance,
          "active": patient.active,
          "historic": patient.historic,
        }}, (err, result) => {
            if(err) {
                console.log(err);
                client.close();
                res.status(400).send(JSON.stringify({error: err}));
            }else
            {
                client.close();
                res.status(200).send(JSON.stringify({success: result}));
            }

        });
    })
    } catch (err) {
      next(err);
    }
  });

  app.post("/patient/medical/add/:id_patient", async (req, res, next) => {
    try {
      const medical: MedicalRecord = req.body;
      const id = req.params.id_patient;
      mongoClient.connect(url, (err, client) => {
        if (err) {
            console.error(err)
            return
        }
        console.log("Connected successfully to server");
        const db = client.db('medx')
        const collection = db.collection('User')
        console.log(id);
        collection.update({"_id": new ObjectId(id)}, { $push: { "historic": medical }}, (err, result) => {
            if(err) {
                client.close();
                res.status(400).send(JSON.stringify({error: err}));
            }else
            {
                client.close();
                res.status(200).send(JSON.stringify({success: result}));
            }

        });
    })
    } catch (err) {
      next(err);
    }
  });

  app.delete("/patient/delete/:id", async (req, res) => {
    const id = req.params.id;
    mongoClient.connect(url, (err, client) => {
        if (err) {
          console.error(err)
          return
        }
        console.log("Connected successfully to server");
        const db = client.db('medx')
        const collection = db.collection('User')
        collection.deleteOne({"_id": new ObjectId(`${id}`)}, (err, result) => {
            if(err) {
                client.close();
                res.status(400).send(JSON.stringify({error: err}));
            }

            client.close();
            res.status(200).send(JSON.stringify({success: result}));
        });
    })
  });
};

export default initializeRoutes;
