import { Group } from "../models/Group";
import { Permission } from "../models/Permission";
import { UserProfile } from '../models/UserProfile';

const util = require( 'util' );

const initializeRoutes = async (app, connection) => {
  const pegaUsersPergroup = async (group, connection): Promise<Group> => {
    console.log("ID RECEIVED IN FUNCTION: ", group.ID_GROUP);
    let sql = `CALL GET_USERS_GROUPS_ALL(${group.ID_GROUP})`;
    const query = util.promisify(connection.query).bind(connection);
    try {
      const groups = await query(sql);
      console.log(groups[0]);
      const groupReturn: Group = {
        id: group.ID_GROUP,
        name: group.GRP_NAME,
        description: group.GRP_DESCRIPTION,
        active: group.ACTIVE,
        arrayIDUser: groups[0],
        arrayPermissions: [
          group.FNC01,
          group.FNC02,
          group.FNC03,
          group.FNC04,
          group.FNC05,
          group.FNC06,
          group.FNC07,
          group.FNC08,
          group.FNC09,
          group.FNC10,
          group.FNC11,
          group.FNC12,
          group.FNC13,
          group.FNC14,
          group.FNC15,
          group.FNC16,
          group.FNC17,
          group.FNC18,
          group.FNC19,
          group.FNC20,
          group.FNC21,
          group.FNC22,
          group.FNC23
        ]
      };
      return groupReturn;
    } catch ( err ) {
      console.log("ERROR WHEN GET GROUPS: ", err);
    }
  };

  app.get("/groups", async (req, res, next) => {

    let sql = `CALL GET_ALL_GROUP_PERMISSIONS()`;
    connection.query(sql, true, async (err, result, field) => {
      let count = 0;
      if (err) {
        res.status(400).send(`Groups not found: ${err}`);
      }
      const retorno = [];
      if (result) {
        const groupList = result[0];
        for (let i = 0; i < groupList.length; i++) {
          const element = groupList[i];
          const resultado = await pegaUsersPergroup(element, connection);
          retorno.push(resultado);
        }
        res.status(200).send(retorno);
      } else {
        res.status(400).send(`Groups not found: ${err}`);
      }
    });
    // });
  });

  app.get("/group/:id", async (req, res) => {
    const idGroup = req.params.id;
    const query = util.promisify(connection.query).bind(connection);
    try {
      if(idGroup) {
        let sql = `CALL GET_SINGLE_GROUP_PERMISSIONS(${idGroup})`;
        console.log('sql', sql);
        const result = await query(sql);
        if(result) {
          console.log("PARAM FOR GET USER: ", result[0][0]);
          const resultado = await pegaUsersPergroup(result[0][0], connection);
          res.status(200).send(resultado);
        }
      }
    } catch (error) {
      console.log('error when get single group');
      res.status(401).send({error: "group not exist"});
    }
    // });
  });

  app.post("/group/add", async (req, res) => {
    const group: Group = req.body;
    // pool.getConnection((err, connection) => {
    //   if (err) return res.status(400).send(`Sintaxe error: ${err}`);

    let sql = `CALL ADD_GROUP('${group.name}','${group.description}',${group.active},
          ${group.arrayPermissions[0]},${group.arrayPermissions[1]},${group.arrayPermissions[2]},${group.arrayPermissions[3]},${group.arrayPermissions[4]},
          ${group.arrayPermissions[5]},${group.arrayPermissions[6]},${group.arrayPermissions[7]},${group.arrayPermissions[8]},${group.arrayPermissions[9]},
          ${group.arrayPermissions[10]},${group.arrayPermissions[11]},${group.arrayPermissions[12]},${group.arrayPermissions[13]},${group.arrayPermissions[14]},
          ${group.arrayPermissions[15]},${group.arrayPermissions[16]},${group.arrayPermissions[17]},${group.arrayPermissions[18]},${group.arrayPermissions[19]},
          ${group.arrayPermissions[20]},${group.arrayPermissions[21]},${group.arrayPermissions[22]})`;
    connection.query(sql, true, (err, result, field) => {
      if (err) {
        res.status(404).send(`Group not added: ${err}`);
      } else {
        res.status(200).send({ success: true });
      }
    });
    // });
  });

  app.put("/group/edit", async (req, res) => {
    const group: Group = req.body;
    // pool.getConnection((err, connection) => {
    //   if (err) return res.status(400).send(`Sintaxe error: ${err}`);

    let sql = `CALL UPDATE_PERMISSIONS(${group.id},
      ${group.arrayPermissions[0]},${group.arrayPermissions[1]},${group.arrayPermissions[2]},${group.arrayPermissions[3]},${group.arrayPermissions[4]},
      ${group.arrayPermissions[5]},${group.arrayPermissions[6]},${group.arrayPermissions[7]},${group.arrayPermissions[8]},${group.arrayPermissions[9]},
      ${group.arrayPermissions[10]},${group.arrayPermissions[11]},${group.arrayPermissions[12]},${group.arrayPermissions[13]},${group.arrayPermissions[14]},
      ${group.arrayPermissions[15]},${group.arrayPermissions[16]},${group.arrayPermissions[17]},${group.arrayPermissions[18]},${group.arrayPermissions[19]},
      ${group.arrayPermissions[20]},${group.arrayPermissions[21]},${group.arrayPermissions[22]}); 
      CALL UPDATE_GROUP(${group.id}, '${group.name}', '${group.description}')`;
      console.log("UPDATE GROUP: ", group);
    connection.query(sql, true, (err, result, field) => {
      if (err) {
        res.status(404).send(`GROUP not found: ${err}`);
      } else {
        res.status(200).send({ success: true });
      }
    });
    // });
  });

  app.delete("/group/delete/:id", async (req, res) => {
    const id = req.params.id;
    // pool.getConnection((err, connection) => {
    //   if (err) return res.status(400).send(`Sintaxe error: ${err}`);

    let sql = `CALL ALTER_ACTIVE_GROUP(${id}, ${false})`;
    connection.query(sql, true, (err, result, field) => {
      // connection.release();
      if (err) {
        res.status(404).send(`Group not found: ${err}`);
      } else {
        res.status(200).send({ success: true });
      }
    });
    // });
  });

  app.put("/group/associateUser", async (req, res) => {
    const groupAssociation = req.body;
    console.log(groupAssociation.arrayIDUserOld);

    if (
      //Novos usuários
      groupAssociation.arrayIDUserOld.length === 0 &&
      groupAssociation.arrayIDUser.length > 0
    ) {
      groupAssociation.arrayIDUser.map((user, index) => {
        let sql = `CALL BIND_USER_GROUP(${groupAssociation.id}, '${user}')`;
        connection.query(sql, true, (err, result, field) => {
          // connection.release();
          if (err) {
            res.status(404).send(`Group not found: ${err}`);
          } else {
          }
        });
        if (index == groupAssociation.arrayIDUser.length - 1) {
          res.status(200).send({ success: true });
        }
      });
    } else if (
      //Update usuários
      groupAssociation.arrayIDUserOld.length > 0 &&
      groupAssociation.arrayIDUser.length > 0
    ) {
      let flagAdd = false;
      let flagRemove = false;
      const intersectionRemove = groupAssociation.arrayIDUserOld.filter(
        user => !groupAssociation.arrayIDUser.includes(user)
      );
      const intersectionAdd = groupAssociation.arrayIDUser.filter(
        user => !groupAssociation.arrayIDUserOld.includes(user)
      );

      console.log("intersectionRemove:", intersectionRemove);
      console.log("intersectionAdd:", intersectionAdd);

      if (intersectionRemove.length > 0) {
        console.log("REMOVE");
        intersectionRemove.map((user, index) => {
          let sql = `CALL BIND_USER_GROUP(4, '${user}')`;
          connection.query(sql, true, (err, result, field) => {
            // connection.release();
            if (err) {
              res.status(404).send(`Group not found: ${err}`);
            } else {
            }
          });
          console.log(
            "index: ",
            index,
            "lengh removed: ",
            intersectionRemove.length
          );
          if (index == intersectionRemove.length - 1) {
            flagRemove = true;
          }
        });
      } else {
        flagRemove = true;
      }

      if (intersectionAdd.length > 0) {
        console.log("ADD");
        intersectionAdd.map((user, index) => {
          let sql = `CALL BIND_USER_GROUP(${groupAssociation.id}, '${user}')`;
          connection.query(sql, true, (err, result, field) => {
            // connection.release();
            if (err) {
              res.status(404).send(`Group not found: ${err}`);
            } else {
            }
          });
          console.log("index: ", index, "lengh add: ", intersectionAdd.length);
          if (index == intersectionAdd.length - 1) {
            flagAdd = true;
          }
        });
      } else {
        flagAdd = true;
      }
      if (flagRemove === true && flagAdd === true) {
        res.status(200).send({ success: true });
      }
    } else if (
      groupAssociation.arrayIDUserOld.length > 0 &&
      groupAssociation.arrayIDUser.length === 0
    ) {
      groupAssociation.arrayIDUserOld.map((user, index) => {
        let sql = `CALL BIND_USER_GROUP(4, '${user}')`;
        connection.query(sql, true, (err, result, field) => {
          // connection.release();
          if (err) {
            res.status(404).send(`Group not found: ${err}`);
          } else {
          }
        });
        if (index == groupAssociation.arrayIDUserOld.length - 1) {
          res.status(200).send({ success: true });
        }
      });
    }
  });

  app.get("/functionalities", async (req, res) => {
    // pool.getConnection((err, connection) => {
    //   if (err) return res.status(400).send(`Sintaxe error: ${err}`);

    let sql = `CALL GET_FUNCTIONALITIES()`;
    connection.query(sql, true, (err, result, field) => {
      // connection.release();
      if (err) {
        res.status(400).send(`Functionalities not found: ${err}`);
      } else {
        const permissions: Permission[] = result[0].map(item => {
          return {
            id: item.ID_FUNCTIONALITIES,
            description: item.FNC_DESCRIPTION,
            active: false
          };
        });
        res.status(200).send(permissions);
      }
    });
  });
  app.get("/getAllDoctors/:id_op&:id_est"), (req, res) => {
    let sql = `CALL  GET_PROFOPS_PROFHS(${req.id_op}, ${req.id_est})`;
    connection.query(sql, true, (err, result, field) => {
      if (err) {
        res.status(400).send(`Doctors not found: ${err}`);
      } else {

        const users: UserProfile[] = [];

        result[0].map(item => {
          users.push({
            id: item.ID_PROFH,
            name: item.PROFH_NAME,
            username: item.USERNAME,
            pass: item.PASS,
            userType: item.USER_TYPE,
            crm: item.CRM,
            occupationArea: undefined,
            specialty: undefined,
            rqe: item.RQE,
            responsability: undefined,
            userGroup: item.ID_GROUP,
            active: item.ACTIVE,
            address: [
              {
                id: item.ID_ESTABLISHMENT,
                neighborhood: item.NEIGHBOURHOOD,
                cep: item.CEP,
                city: item.CITY,
                complement: item.ADDITIONAL,
                state: item.STATE,
                nomeLocal: item.ESTP_NAME,
                numberPlace: item.ADDRESS_NUMBER,
                publicPlace: item.STREET
              }
            ]
          })

          }
        )

        res.status(200).send(users);
      }
    });    
  }
};

export default initializeRoutes;
