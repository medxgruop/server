import userRoutes from "./routes/user-routes";
import groupRoutes from "./routes/group-routes";
import patientRoutes from "./routes/patient-routes";
import schedulerRoutes from "./routes/scheduler-routes";

const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const mysql = require("mysql");
const mongoClient = require('mongodb').MongoClient
const mongo = require('mongodb');
const port = process.env.PORT || 3002;
const URL_SERVER = `medx-mongodb.documents.azure.com:10255`;

const pool = mysql.createPool({
  connectionLimit: 10, // default = 10
  host: 'medx-database-mysql.mysql.database.azure.com',
  user: 'medxadmin@medx-database-mysql',
  password: "password@123",
  database: "medx_database",
  multipleStatements: true,
  port: 3306,
  ssl: true,
  timeout: 60000,
});

let db_config = {
  host: 'medx-database-mysql.mysql.database.azure.com',
  user: 'medxadmin@medx-database-mysql',
  password: 'password@123',
  database: 'medx_database',
  port: 3306,
  ssl: true,
  timeout: 60000,
};

// let connection;

// function handleDisconnect() {
//   connection = new mysql.createConnection(db_config);

//   connection.connect(err => {
//     if (err) {
//       console.log("!!! Cannot connect !!! Error:");
//       throw err;
//     }
//     else
//     {
//        console.log("Connection established.");
//     }
//     // if (err) {
//     //   console.log("error when connecting to db:", err);
//     //   setTimeout(handleDisconnect, 2000);
//     // }
//     // else {
//     //   console.log('connected as id ' + connection.threadId);
//     // }
//   });
//   connection.on("error", err => {
//     console.log("db error", err);
//     handleDisconnect(); // lost due to either server restart, or a
//   });
// }

// handleDisconnect();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.listen(port, () =>
  console.log(`Server UserWeb is running in port: ${port}`)
);

//route files
userRoutes(app, pool);
groupRoutes(app, pool);
patientRoutes(app, pool, mongoClient, mongo, URL_SERVER);
schedulerRoutes(app, pool, mongoClient, mongo, URL_SERVER);
